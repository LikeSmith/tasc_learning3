"""
BasicAbstractLearner_SI.py

Learn a Basic abstraction for a single integrator swarm
"""

import numpy as np
import matplotlib.pyplot as plt
from tqdm import trange

from youngknight.frameworks.AbstractionEncoder import AbstractEmbedder
from youngknight.frameworks.AbstractionEncoder.encoders import BasicEncoder
from youngknight.frameworks.AbstractionEncoder.dynamics import BasicDynamics
from youngknight.core import rand_vectors

max_swarm_size = 10
min_swarm_size = 10
abst_state_size = 4
prnt_state_size = 2
chld_state_size = 2
param_size = 1

K_p = [-3.1, -3.3]
K_s = [10.0, 1.0]
test_batch_size = 1
test_swarm_size = 10
t_f = 10.0

del_t = 0.01
J = 1.5
gamma_1 = 0.01
gamma_2 = 1000
gamma_3 = 700
gamma_4 = 0.07
gamma_5 = 1000
gamma_6 = 0.5

theta_min = -0.2
theta_max = 0.2
omega_min = -1.0
omega_max = 1.0

p_min = -2.0
p_max = 2.0
v_min = -1.0
v_max = 1.0
m_min = 0.25
m_max = 0.75

home = 'results/'

dyn_params = {}
dyn_params['hidden_size'] = 200
dyn_params['hidden_activ'] = 'leakyrelu_0.1'
dyn_params['init_var'] = 0.1

enc_params = {}
enc_params['max_swarm_size'] = max_swarm_size
enc_params['init_var'] = 0.1
enc_params['h_size'] = 200
enc_params['activ'] = 'leakyrelu_0.1'

trn_params = {}
trn_params['max_swarm_size'] = max_swarm_size
trn_params['min_swarm_size'] = min_swarm_size
trn_params['variable_size_swarm'] = False
trn_params['multiplicity'] = 2
trn_params['learning_rate'] = 0.001
trn_params['n_epochs'] = 100
trn_params['n_batches'] = 1000
trn_params['batch_size'] = 256
trn_params['val_size'] = 1024
trn_params['verbosity'] = 2
trn_params['swarm_update_rate'] = 100
trn_params['use_best'] = True
trn_params['patience'] = 10
trn_params['regen_data'] = True
trn_params['swrm_state_gen'] = lambda n,s: rand_vectors((n, s), np.array([p_min, v_min]), np.array([p_max, v_max]))
trn_params['swrm_param_gen'] = lambda n,s: rand_vectors((n, s), np.array([m_min]), np.array([m_max]))
trn_params['prnt_state_gen'] = lambda n: rand_vectors(n, np.array([theta_min, omega_min]), np.array([theta_max, omega_max]))

def f_smooth_stribeck(v):
    return gamma_1*(np.tanh(gamma_2*v) - np.tanh(gamma_3*v)) + gamma_4*np.tanh(gamma_5*v) + gamma_6*v

def plane_step(prnt_state, swrm_state, swrm_param):
    theta = prnt_state[:, 0]
    omega = prnt_state[:, 1]
    
    p = swrm_state[:, :, 0]
    v = swrm_state[:, :, 1]
    
    m = swrm_param[:, :, 0]
    
    batch_size = swrm_state.shape[0]
    swrm_size = swrm_state.shape[1]
    
    tau_s = np.zeros(batch_size)
    J_s = np.zeros(batch_size)
    J_s_dot = np.zeros(batch_size)
    
    for i in range(swrm_size):
        tau_s += m[:, i]*p[:, i]
        J_s += m[:, i]*p[:, i]**2
        J_s_dot += 2*m[:, i]*p[:, i]*v[:, i]
        
    new_alpha = -(9.81*np.cos(theta)*tau_s + omega*J_s_dot + f_smooth_stribeck(omega))/(J + J_s)
    #new_omega = np.clip(omega + del_t*new_alpha, omega_min, omega_max)
    #new_theta = np.clip(theta + del_t*new_omega, theta_min, theta_max)
    new_omega = omega + del_t*new_alpha
    new_theta = theta + del_t*new_omega
    
    return np.stack((new_theta, new_omega), axis=1)

def swarm_step(swrm_state, u_s, swrm_params):    
    p = swrm_state[:, :, 0]
    #v = np.clip(u_s[:, :, 0], v_min, v_max)
    #p_new = np.clip(p + del_t*v, p_min, p_max)
    v = u_s[:, :, 0]
    p_new = p + del_t*v
    
    return np.stack((p_new, v), axis=2)

def ctrl(prnt_state, swrm_state, swrm_params):
    batch_size = prnt_state.shape[0]
    swarm_size = swrm_state.shape[1]
    
    theta = prnt_state[:, 0]
    omega = prnt_state[:, 1]
    
    p = swrm_state[:, :, 0]
    v = swrm_state[:, :, 1]
    
    m = swrm_params[:, :, 0]
    
    tau_s = np.zeros(batch_size)
    tau_s_dot = np.zeros(batch_size)
    J_s = np.zeros(batch_size)
    J_s_dot = np.zeros(batch_size)
    S_0 = np.zeros(batch_size)
    S_1 = np.zeros(batch_size)
    S_2 = np.zeros(batch_size)
    S_3 = np.zeros(batch_size)
    
    for i in range(swarm_size):
        tau_s += 9.81*m[:, i]*p[:, i]
        tau_s_dot += 9.81*m[:, i]*v[:, i]
        J_s += m[:, i]*p[:, i]**2
        J_s_dot += 2*m[:, i]*p[:, i]*v[:, i]
        S_0 += m[:, i]**2
        S_1 += m[:, i]**2*p[:, i]
        S_2 += m[:, i]**2*p[:, i]**2
        
        for j in range(i+1, swarm_size):
            S_3 += m[:, i]**2*m[:, j]**2*(p[:, i] - p[:, j])**2
            
    tau_d = -K_p[0]*theta - K_p[1]*omega
    tau_d_dot = -K_p[0]*omega - K_p[1]*(-np.cos(theta)*tau_s - omega*J_s_dot - f_smooth_stribeck(omega))/(J + J_s)
    J_d = 0.0125*tau_d**2 + 0.025
    J_d_dot = 2*0.0125*tau_d*tau_d_dot
    
    tau_dot = K_s[0]*(tau_d - tau_s) + tau_d_dot
    J_dot = K_s[1]*(J_d - J_s) + J_d_dot
    
    u_s = np.zeros((batch_size, swarm_size, 1))
    
    for i in range(swarm_size):
        phi_1 = m[:, i]*S_2 - m[:, i]*p[:, i]*S_1
        phi_2 = 0.5*(m[:, i]*p[:, i]*S_0 - m[:, i]*S_1)
        
        u_s[:, i, 0] = (phi_1*tau_dot + phi_2*J_dot)/S_3
        
    return np.clip(u_s, v_min, v_max)

if __name__ == '__main__':
    print('Setting up networks...')
    abs_enc = BasicEncoder(abst_state_size, chld_state_size, param_size, params=enc_params, home=home, name='encoder')
    abs_dyn = BasicDynamics(abst_state_size, prnt_state_size, params=dyn_params, home=home, name='dynamics')
    embedder = AbstractEmbedder(abs_enc, abs_dyn, plane_step, trn_params, name='AbstractEncoder')
    
    print('training...')
    hist = embedder.train()
    
    print('saving...')
    abs_enc.save('basic_abs_enc.pkl')
    abs_dyn.save('basic_abs_dyn.pkl')
    
    print('testing results...')
    s_p_0 = trn_params['prnt_state_gen'](test_batch_size)
    s_s_0 = trn_params['swrm_state_gen'](test_batch_size, test_swarm_size)
    p_s = trn_params['swrm_param_gen'](test_batch_size, test_swarm_size)
    
    t = np.arange(0.0, t_f+del_t, del_t)
    s_p_true = np.zeros((test_batch_size, t.shape[0], prnt_state_size))
    s_p_pred = np.zeros((test_batch_size, t.shape[0], prnt_state_size))
    s_s = np.zeros((test_batch_size, test_swarm_size, t.shape[0], chld_state_size))
    u_s = np.zeros((test_batch_size, test_swarm_size, t.shape[0]-1, 1))
    a_s = np.zeros((test_batch_size, t.shape[0]-1, abst_state_size))
    
    s_p_true[:, 0, :] = s_p_0
    s_p_pred[:, 0, :] = s_p_0
    s_s[:, :, 0, :] = s_s_0
    
    for i in trange(t.shape[0]-1):
        u_s[:, :, i, :] = ctrl(s_p_true[:, i, :], s_s[:, :, i, :], p_s)
        s_s[:, :, i+1, :] = swarm_step(s_s[:, :, i, :], u_s[:, :, i, :], p_s)
        s_p_true[:, i+1, :] = plane_step(s_p_true[:, i, :], s_s[:, :, i+1, :], p_s)
        
        a_s[:, i, :] = embedder.encode(s_s[:, :, i+1, :], p_s)
        s_p_pred[:, i+1, :] = embedder.step(s_p_true[:, i, :], a_s[:, i, :])
        
    plt.figure()
    plt.plot(hist['ave_loss'])
    plt.plot(hist['val_loss'])
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend(['Training', 'Validation'])
    
    plt.figure()
    plt.plot(t, s_p_true[0, :, 0], 'b--')
    plt.plot(t, s_p_pred[0, :, 0], 'b')
    plt.plot(t, s_p_true[0, :, 1], 'r--')
    plt.plot(t, s_p_pred[0, :, 1], 'r')
    plt.xlabel('time (s)')
    plt.ylabel('(rad, rad/s)')
    plt.legend(['theta true', 'theta pred', 'omega true', 'omega pred'])
    
    plt.show()
    
    

