'''
train_plane_alone.py

Experiment to use A3C to learn policy for Plane System alone, outside of the 
swarm parent system
'''

import numpy as np
import matplotlib.pyplot as plt
import _pickle as pkl

from youngknight.sims import PlaneSystem
from youngknight.frameworks.a3c.actors import Basic_Actor_Cont
from youngknight.frameworks.a3c import A3C
from youngknight.core import quad

dt = 0.05
home = 'results/'
log_dir = 'logs'
glb_filename = 'global_actor.pkl'
loc_filename = 'local_actor%d.pkl'

Q = np.diag([1.0, 0.1])
R = np.diag([0.001])

plane_limits = {}
plane_limits['theta_min'] = -0.5
plane_limits['theta_max'] = 0.5
plane_limits['omega_min'] = -1.0
plane_limits['omega_max'] = 1.0
plane_limits['mu_min'] = -5.0
plane_limits['mu_max'] = 5.0
plane_limits['J_min'] = 1.0
plane_limits['J_max'] = 5.0
plane_limits['J_dot_min'] = -5.0
plane_limits['J_dot_max'] = 5.0
plane_limits['J_p_min'] = 1.0
plane_limits['J_p_max'] = 1.0
plane_limits['gamma_1_min'] = 0.01
plane_limits['gamma_1_max'] = 0.01
plane_limits['gamma_2_min'] = 1000
plane_limits['gamma_2_max'] = 1000
plane_limits['gamma_23_min'] = 300
plane_limits['gamma_23_max'] = 300
plane_limits['gamma_14_min'] = -0.01
plane_limits['gamma_14_max'] = -0.01
plane_limits['gamma_5_min'] = 1000
plane_limits['gamma_5_max'] = 1000
plane_limits['gamma_6_min'] = 0.5
plane_limits['gamma_6_max'] = 0.5

actor_params = {}
actor_params['hidden_size_policy'] = 200
actor_params['hidden_size_value'] = 100
actor_params['activ_policy'] = 'leakyrelu_0.1'
actor_params['activ_value'] = 'leakyrelu_0.1'
actor_params['min_a'] = [-5]
actor_params['max_a'] = [5]
actor_params['pol_learning_rate'] = 0.0001
actor_params['val_learning_rate'] = 0.001
actor_params['zero_reg'] = 0
actor_params['use_l1_loss'] = False
actor_params['use_l2_loss'] = False
actor_params['use_bias'] = False
actor_params['T_max'] = 1000
actor_params['t_max'] = 200
actor_params['global_update_rate'] = 10
actor_params['validation_rate'] = 10
actor_params['use_es'] = False
actor_params['gamma'] = 0.9
actor_params['beta_entropy'] = 0.01
actor_params['sig_min'] = 1e-4
actor_params['use_noise'] = False
actor_params['verbosity'] = 2

def rwd(state, state1, action, k, params):
    cost = quad(state, Q) + quad(action, R)
    return -k*cost

if __name__ == '__main__':
    print('setting up simulator...')
    sim = PlaneSystem(plane_limits, dt=dt, enforce_limits=False, full_state_feedback=True, ctrl=None, obs=None, rwd=rwd, done=None, name='Plane', mode=1)
    actor_params['sim_params'] = sim.param_gen(1)
    
    print('initializing_framework...')
    learner = A3C(sim, Basic_Actor_Cont, actor_params, use_gym=False, use_obs=False, home=home, name='PlaneAlone_A3C', log_dir=log_dir)
    
    print('starting_actors...')
    learner.start_actors(with_val=True)
    learner.join(use_best=True)
    
    learner.save_results(glb_filename)
    learner.save_actors(loc_filename)
    
    print('training finished!')
    hists = learner.get_histories()
    glb_hist = learner.get_val_history()
    
    pkl.dump([hists, glb_hist, learner.get_glb_r(), actor_params['sim_params']], open(home+'training_results.pkl', 'bw'))
    
    plt.figure()
    plt.plot(learner.get_glb_r())
    plt.plot(glb_hist['ep_stamp'], glb_hist['running_ep_r'])
    plt.xlabel('global episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['running_ep_r'])))
    plt.xlabel('local episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_p'])))
    plt.xlabel('update number')
    plt.ylabel('policy loss')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_v'])))
    plt.xlabel('update number')
    plt.ylabel('value loss')
    
    ctrl = lambda state, k, params, abs_state: learner.get_final_actor().policy(state)
    sim = PlaneSystem(plane_limits, dt=dt, enforce_limits=True, full_state_feedback=True, ctrl=ctrl, rwd=rwd, name='Plane', mode=1)
    
    x_0 = sim.init_state_gen(10)
    x_0[0, :] = np.zeros(2)
    
    print('running simulation...')
    n = 200
    res, _ = sim.gen_trajectories(x_0, n, params=actor_params['sim_params'], verbosity=1)
    t = np.arange(n+1)*dt
    
    plt.figure()
    plt.plot(t.T, res['states'][:, :, 0].T)
    plt.title('Test Run: Position')
    
    plt.figure()
    plt.plot(t.T, res['states'][:, :, 1].T)
    plt.title('Test Run: Velocity')
    
    plt.figure()
    plt.plot(t[:-1].T, res['actions'][:, :, 0].T)
    
    plt.figure()
    plt.plot(t[:-1].T, res['rewards'].T)
    
    plt.show()