"""
train_swarm_alone_reg_SI.py

Experiment to learn regulation control for single integrator swarm alone
"""

import numpy as np
import matplotlib.pyplot as plt
import _pickle as pkl

from youngknight.sims import SingleIntegratorSwarm
from youngknight.frameworks.a3c.actors import Basic_SwarmActor_Cont
from youngknight.frameworks.a3c import A3C

dt = 0.05
L = 1.0
home = 'results/'
log_dir = 'logs'
glb_filename = 'global_actor_reg_SI.pkl'
loc_filename = 'local_actor%d_reg_SI.pkl'

Q_i = 1.0
R_i = 0.01

sim_limits = {}
sim_limits['p_max'] = 0.25
sim_limits['p_min'] = -0.25
sim_limits['v_max'] = 1.0
sim_limits['v_min'] = -1.0
sim_limits['m_max'] = 0.5
sim_limits['m_min'] = 0.25

actor_params = {}
actor_params['swarm_size'] = 10
actor_params['max_a'] = [1.0]
actor_params['min_a'] = [-1.0]
actor_params['hidden_size_policy'] = 200
actor_params['hidden_size_value'] = 100
actor_params['activ_policy'] = 'leakyrelu_0.1'
actor_params['activ_value'] = 'leakyrelu_0.1'
actor_params['use_bias'] = False
actor_params['use_l1_loss_pol'] = False
actor_params['use_l2_loss_pol'] = False
actor_params['use_l1_loss_val'] = False
actor_params['use_l2_loss_val'] = False
actor_params['zero_reg'] = 0
actor_params['beta_entropy'] = 0.01
actor_params['sig_min'] = 1e-4
actor_params['T_max'] = 1000
actor_params['t_max'] = 200
actor_params['pol_learning_rate'] = 0.0001
actor_params['val_learning_rate'] = 0.001
actor_params['global_update_rate'] = 10
actor_params['validation_rate'] = 10
#actor_params['validation_patience'] = 100
actor_params['gamma'] = 0.9
actor_params['use_noise'] = False
actor_params['verbosity'] = 2

def rwd(state, state1, action, k, params, des_traj):
    p = state[:, 0]
    v = action[:, 0]
    
    cost = Q_i*p**2 + R_i*v**2
    
    return -cost

if __name__ == '__main__':
    print('setting up simulator...')
    sim = SingleIntegratorSwarm(limits=sim_limits, del_t=dt, L=L, enforce_limits=False, full_state_feedback=False, rwd=rwd, name='SwarmAlone_reg_SI')
    actor_params['sim_params'] = sim.param_gen(1, actor_params['swarm_size'])
    
    print('initializing_framework...')
    learner = A3C(sim, Basic_SwarmActor_Cont, actor_params, use_gym=False, use_obs=True, home=home, name='SwarmAlone_reg_SI_A3C', log_dir=log_dir)
    
    #learner.actors[0].train()
    print('starting_actors...')
    learner.start_actors(with_val=True)
    learner.join()
    
    print('training finished!')
    learner.save_results(glb_filename)
    learner.save_actors(loc_filename)
    
    hists = learner.get_histories()
    glb_hist = learner.get_val_history()
    
    pkl.dump([hists, glb_hist, learner.get_glb_r(), actor_params['sim_params']], open(home+'training_reg_SI_results.pkl', 'bw'))
    
    plt.figure()
    plt.plot(learner.get_glb_r())
    plt.plot(glb_hist['ep_stamp'], glb_hist['running_ep_r'])
    plt.xlabel('global episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['running_ep_r'])))
    plt.xlabel('local episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_p'])))
    plt.xlabel('update number')
    plt.ylabel('policy loss')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_v'])))
    plt.xlabel('update number')
    plt.ylabel('value loss')
    
    print('Generating test trajectory...')
    ctrl = lambda state, k, params, abs_state, aux_state: learner.get_final_actor().policy(state, k, int(params[0, 2]))
    sim = SingleIntegratorSwarm(limits=sim_limits, del_t=dt, L=L, enforce_limits=False, full_state_feedback=False, rwd=rwd,ctrl=ctrl, name='SwarmAlone_reg_SI_test')
    
    x_0 = sim.init_state_gen(1, actor_params['swarm_size'])
    
    n = 200
    res, _ = sim.gen_trajectories(x_0, n, params=actor_params['sim_params'], verbosity=1)
    t = np.arange(n+1)*dt
    
    plt.figure()
    plt.plot(t, res['states'][0, :, :, 0])
    plt.title('Test Run: Position')
    
    plt.figure()
    plt.plot(t[:-1], res['actions'][0, :, :, 0])
    plt.title('Test Run: Velocity')
    
    plt.figure()
    plt.plot(t[:-1], res['rewards'][0, :])
    
    plt.show()