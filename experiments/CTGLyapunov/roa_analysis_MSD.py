"""
roa_analysis_MSD.py

Region of Attraction estimation
"""

import numpy as np
from scipy import ndimage
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm

from youngknight.frameworks.Shooting.policies import LinearPolicy
from youngknight.frameworks.Shooting.ctg import BasicCTG
from youngknight.models.Dynamics import LinMSD

import params.exp_MSD2 as exp

home = 'results/'
pol_filename = 'Lin_Pol_%s.pkl'%exp.exp_id
ctg_filename = 'Lin_CTG_%s.pkl'%exp.exp_id
C_filename = 'C_fun_Lin_Pol_%s.png'%exp.exp_id
Diffs_filename = 'Diffs_Lin_Pol_%s.png'%exp.exp_id
Edges_filename = 'Edges_Lin_Pol_%s.png'%exp.exp_id

tau = 0.01

if __name__ == '__main__':
    print('Setting up Models...')
    dyn = LinMSD(params=exp.dyn_params, home=home, name='pend_model_dyn')

    state_size = dyn.state_size
    actin_size = dyn.actin_size
    param_size = dyn.param_size

    pol = LinearPolicy(state_size, actin_size, param_size, load_file=pol_filename, home=home, name='basic_pol')
    ctg = BasicCTG(state_size, param_size, load_file=ctg_filename, home=home, name='basic_ctg')

    print('Setting up grids...')
    p_axis_pos = np.arange(tau, exp.p_max_test, 2*tau)
    p_axis_neg = np.arange(exp.p_min_test, -tau, 2*tau) + tau
    p_axis = np.concatenate([p_axis_neg, np.zeros(1), p_axis_pos])

    v_axis_pos = np.arange(tau, exp.v_max_test, 2*tau)
    v_axis_neg = np.arange(exp.v_min_test, -tau, 2*tau) + tau
    v_axis = np.concatenate([v_axis_neg, np.zeros(1), v_axis_pos])

    grid_p, grid_v = np.meshgrid(p_axis, v_axis, indexing='ij')

    corners_p = np.concatenate([p_axis_neg-tau, np.zeros(1), p_axis_pos+tau])
    corners_v = np.concatenate([v_axis_neg-tau, np.zeros(1), v_axis_pos+tau])
    corners_p, corners_v = np.meshgrid(corners_p, corners_v, indexing='ij')

    s = np.zeros((p_axis.shape[0]*v_axis.shape[0], 2))
    p = exp.param_gen(p_axis.shape[0]*v_axis.shape[0])

    for i in range(p_axis.shape[0]):
        for j in range(v_axis.shape[0]):
            s[i*v_axis.shape[0]+j, 0] = grid_p[i, j]
            s[i*v_axis.shape[0]+j, 1] = grid_v[i, j]

    print('Evaluating Lyap funciton...')
    a = pol.pol(s, p, 0)
    s_ = dyn.step(s, a, p, 0)
    C_ = ctg.ctg(s_, p, 0)
    C = ctg.ctg(s, p, 0)
    C_diff = C_ - C

    ctg_weights = ctg.save_weights()
    pol_weights = pol.save_weights()
    A = np.array([[1-exp.dt**2*p[0, 1]/p[0, 0], exp.dt-exp.dt**2*p[0, 2]/p[0, 0]], [-exp.dt*p[0, 1]/p[0, 0], 1-exp.dt*p[0, 2]/p[0, 0]]])

    L_c = np.linalg.norm(ctg_weights['W1'], ord=np.inf)*np.linalg.norm(ctg_weights['W2'], ord=np.inf)
    L_p = np.linalg.norm(pol_weights['K'], ord=np.inf)
    L_f = np.linalg.norm(A, ord=np.inf)
    L_g = L_f*(L_p + 1)
    thresh = L_c*(L_g + 1)*tau*0.1
    #thresh = 0.5

    grid_C = np.zeros(grid_p.shape)
    grid_C_diff = np.zeros(grid_p.shape)
    grid_centers = np.zeros((grid_p.shape[0]-1, grid_p.shape[1]-1, 2))
    grid_s = np.zeros((grid_p.shape[0]-1, grid_p.shape[1]-1))
    grid_C2 = np.zeros((grid_p.shape[0]-1, grid_p.shape[1]-1))

    print('Sorting results...')
    for i in range(p_axis.shape[0]):
        for j in range(v_axis.shape[0]):
            grid_C[i, j] = C[i*v_axis.shape[0]+j, 0]
            grid_C_diff[i, j] = C_diff[i*v_axis.shape[0]+j, 0]

            if grid_p[i, j] != 0 and grid_v[i, j] != 0:
                if grid_p[i, j] > 0:
                    if grid_v[i, j] > 0:
                        grid_C2[i-1, j-1] = grid_C[i, j]
                        grid_centers[i-1, j-1, 0] = p_axis[i]
                        grid_centers[i-1, j-1, 1] = v_axis[j]
                    else:
                        grid_C2[i-1, j] = grid_C[i, j]
                        grid_centers[i-1, j, 0] = p_axis[i]
                        grid_centers[i-1, j, 1] = v_axis[j]
                else:
                    if grid_v[i, j]> 0:
                        grid_C2[i, j-1] = grid_C[i, j]
                        grid_centers[i, j-1, 0] = p_axis[i]
                        grid_centers[i, j-1, 1] = v_axis[j]
                    else:
                        grid_C2[i, j] = grid_C[i, j]
                        grid_centers[i, j, 0] = p_axis[i]
                        grid_centers[i, j, 1] = v_axis[j]

                if grid_C_diff[i, j] < -thresh:
                    if grid_p[i, j] > 0:
                        if grid_v[i, j] > 0:
                            grid_s[i-1, j-1] = -1
                        else:
                            grid_s[i-1, j] = -1
                    else:
                        if grid_v[i, j] > 0:
                            grid_s[i, j-1] = -1
                        else:
                            grid_s[i, j] = -1
                elif grid_C_diff[i, j] > thresh:
                    if grid_p[i, j] > 0:
                        if grid_v[i, j] > 0:
                            grid_s[i-1, j-1] = 1
                        else:
                            grid_s[i-1, j] = 1
                    else:
                        if grid_v[i, j] > 0:
                            grid_s[i, j-1] = 1
                        else:
                            grid_s[i, j] = 1
    # find edges
    print('Finding Edges...')
    Kx = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], np.float32)
    Ky = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]], np.float32)
    Ix = ndimage.filters.convolve(grid_s, Kx)
    Iy = ndimage.filters.convolve(grid_s, Ky)

    edges = np.hypot(Ix, Iy)
    edges /= edges.max()

    # sort edges (need to get rid of center)
    print('Finding Max V...')
    true_edges = np.zeros(edges.shape)
    V_lim = np.inf

    for i in range(edges.shape[0]):
        for j in range(edges.shape[1]):
            if grid_s[i, j] == -1 and edges[i, j] > 0 and grid_centers[i, j, 0]**2 + grid_centers[i, j, 1]**2 >= 4.0:
                true_edges[i, j] = 1

                if grid_C2[i, j] < V_lim:
                    V_lim = grid_C2[i, j]

    print('Calculating sublevel set bounds...')
    theta = np.arange(-np.pi, np.pi, 0.1)
    roa_bound = np.zeros((theta.shape[0], 2))

    for i in range(theta.shape[0]):
        r_bound = [0, 10]
        while r_bound[1] - r_bound[0] > 0.01:
            r_test = 0.5*(r_bound[0] + r_bound[1])
            s_test = np.array([[r_test*np.cos(theta[i]), r_test*np.sin(theta[i])]])
            V_test = ctg.ctg(s_test, p, 0)

            if V_test > V_lim:
                r_bound[1] = r_test
            else:
                r_bound[0] = r_test

        roa_bound[i, 0] = 0.5*(r_bound[0] + r_bound[1])*np.cos(theta[i])
        roa_bound[i, 1] = 0.5*(r_bound[0] + r_bound[1])*np.sin(theta[i])

    roa_bound = np.concatenate([roa_bound, np.expand_dims(roa_bound[0, :], axis=0)], axis=0)

    y_upper = np.array([exp.v_min_test, exp.v_max_test])
    y_lower = np.array([exp.v_min_test, exp.v_max_test])

    x_upper = (exp.f_max - pol_weights['K'][1, 0]*y_upper)/pol_weights['K'][0, 0]
    x_lower = (exp.f_min - pol_weights['K'][1, 0]*y_lower)/pol_weights['K'][0, 0]

    print('Plotting Results')
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(grid_p, grid_v, grid_C, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    plt.xlabel('Position')
    plt.ylabel('Velocity')
    plt.savefig(home+C_filename)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(grid_p, grid_v, grid_C_diff, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    plt.xlabel('Position')
    plt.ylabel('Velocity')

    fig = plt.figure()
    plt.pcolormesh(corners_p.T, corners_v.T, grid_s.T)
    plt.plot(roa_bound[:, 0], roa_bound[:, 1])
    plt.plot(x_upper, y_upper, 'r')
    plt.plot(x_lower, y_lower, 'r')
    plt.xlabel('position')
    plt.ylabel('velocity')
    plt.savefig(home+Diffs_filename)

    fig = plt.figure()
    plt.pcolormesh(corners_p.T, corners_v.T, edges.T)
    plt.xlabel('position')
    plt.ylabel('velocity')

    fig = plt.figure()
    plt.pcolormesh(corners_p.T, corners_v.T, true_edges.T)
    plt.xlabel('position')
    plt.ylabel('velocity')
    plt.savefig(home+Edges_filename)

    plt.show()
