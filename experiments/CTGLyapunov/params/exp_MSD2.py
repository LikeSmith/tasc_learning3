"""
exp_MSD2.py

Sets up plant and controller parameters for an MSD system and basic controller
"""

import numpy as np
import tensorflow as tf

from youngknight.core.utils import rand_vectors

exp_id = 'MSD2'
dt = 0.05
verbosity = 2
use_best = True
no_regen = False
patience = 10
n_steps= 100

p_min = -4.0
p_max = 4.0
v_min = -2.0
v_max = 2.0
f_min = -5.0
f_max = 5.0

m_min = 1.0
m_max = 1.0
k_min = -1.0
k_max = -1.0
c_min = 0.0
c_max = 0.0

p_min_test = -10.0
p_max_test =  10.0
v_min_test = -10.0
v_max_test =  10.0

init_state_gen = lambda n: rand_vectors(n, [p_min, v_min], [p_max, -v_min])
init_state_gen_test = lambda n: rand_vectors(n, [p_min_test, v_min_test], [p_max_test, -v_min_test])
param_gen = lambda n: rand_vectors(n, [m_min, k_min, c_min], [m_max, k_max, c_max])

trainer_pol = tf.train.AdamOptimizer(learning_rate=0.001)
trainer_ctg = tf.train.AdamOptimizer(learning_rate=0.001)

dyn_params = {}
dyn_params['del_t'] = dt
dyn_params['p_min'] = p_min
dyn_params['p_max'] = p_max
dyn_params['v_min'] = v_min
dyn_params['v_max'] = v_max
dyn_params['f_min'] = f_min
dyn_params['f_max'] = f_max
dyn_params['clip_f'] = True
dyn_params['clip_v'] = False
dyn_params['clip_p'] = False

lqr_params = {}
lqr_params['Q'] = np.array([[1.0, 0.0], [0.0, 0.1]])
lqr_params['R'] = np.array([[0.1]])

basic_pol_params = {}
basic_pol_params['h_size'] = 100
basic_pol_params['use_bias'] = False
basic_pol_params['activ'] = 'leakyrelu_0.1'
basic_pol_params['a_min'] = [f_min]
basic_pol_params['a_max'] = [f_max]
basic_pol_params['init_var'] = 0.1

lin_pol_params = {}
lin_pol_params['use_bias'] = False
lin_pol_params['init_var'] = 0.1

ctg_NN_params = {}
ctg_NN_params['h_size'] = 50
ctg_NN_params['use_bias'] = False
ctg_NN_params['activ'] = 'leakyrelu_0.1'
ctg_NN_params['use_l1_loss'] = False
ctg_NN_params['use_l2_loss'] = True
ctg_NN_params['gamma_l1'] = 0.001
ctg_NN_params['gamma_l2'] = 0.01
ctg_NN_params['init_var'] = 0.1
ctg_NN_params['squared'] = False
ctg_NN_params['trainer'] = trainer_ctg
ctg_NN_params['verbosity'] = verbosity
ctg_NN_params['init_state_gen'] = init_state_gen
ctg_NN_params['param_gen'] = param_gen
ctg_NN_params['no_regen'] = no_regen
ctg_NN_params['patience'] = patience
ctg_NN_params['use_best'] = use_best
ctg_NN_params['n_steps'] = n_steps

lrn_params = {}
lrn_params['n_steps'] = n_steps
lrn_params['gamma'] = 0.99
lrn_params['use_l1_loss'] = False
lrn_params['use_l2_loss'] = False
lrn_params['use_l1_loss_ctg'] = False
lrn_params['use_l2_loss_ctg'] = True
lrn_params['gamma_l1'] = 0.001
lrn_params['gamma_l2'] = 0.001
lrn_params['gamma_l1_ctg'] = 0.001
lrn_params['gamma_l2_ctg'] = 0.001
lrn_params['trainer'] = trainer_pol
lrn_params['trainer_ctg'] = trainer_ctg
lrn_params['verbosity'] = verbosity
lrn_params['init_state_gen'] = init_state_gen
lrn_params['param_gen'] = param_gen
lrn_params['regen_data'] = not no_regen
lrn_params['patience'] = patience
lrn_params['use_best'] = use_best
