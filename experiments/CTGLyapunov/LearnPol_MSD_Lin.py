"""
LearnPol_MSD_Lin.py

Learns a linear policy and Neural Network cost-to-go function independantly using
standard shooting method
"""

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

from youngknight.frameworks.Shooting import Shooting
from youngknight.frameworks.Shooting.policies import LinearPolicy
from youngknight.models.Dynamics import LinMSD
from youngknight.models.Rewards import LQRReward

import params.exp_MSD2 as exp

home = 'results/'
pol_filename = 'Lin_Pol_%s.pkl'%exp.exp_id

n_epochs = 100
n_batches = 1000
batch_size = 128
val_size = 200

if __name__ == '__main__':
    print('Setting up Models...')
    sess = tf.Session()
    dyn = LinMSD(params=exp.dyn_params, home=home, name='MSD_model_dyn', sess=sess)

    state_size = dyn.state_size
    actin_size = dyn.actin_size
    param_size = dyn.param_size

    rwd = LQRReward(state_size, state_size, actin_size, param_size, params=exp.lqr_params, home=home, name='lqr_reward', sess=sess)
    pol = LinearPolicy(state_size, actin_size, param_size, params=exp.lin_pol_params, home=home, name='msd_policy', sess=sess)

    print('Building learner...')
    learner = Shooting(dyn, rwd, pol, params=exp.lrn_params, home=home, name='shooting_learner', sess=sess)

    print('Training Policy...')
    hist_pol = learner.train(n_epochs, n_batches, batch_size, val_size)

    print('Done!')
    print('Saving Results...')
    pol.save(pol_filename)

    t = np.arange(exp.lrn_params['n_steps']+1)*exp.dt

    plt.figure()
    plt.plot(hist_pol['ave_loss_pol'])
    plt.plot(hist_pol['val_loss_pol'])
    plt.xlabel('epoch')
    plt.ylabel('loss_pol')
    plt.legend(['Training', 'Validation'])

    plt.figure()
    plt.plot(t, hist_pol['s'][hist_pol['min_loss_epoch'], :, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('theta (rad)')

    plt.figure()
    plt.plot(t, hist_pol['s'][hist_pol['min_loss_epoch'], :, :, 1].T)
    plt.xlabel('time (s)')
    plt.ylabel('omega (rad/s)')

    plt.figure()
    plt.plot(t[:-1], hist_pol['a'][hist_pol['min_loss_epoch'], :, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('tau (N-m)')
