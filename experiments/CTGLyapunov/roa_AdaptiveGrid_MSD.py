"""
roa_AdaptiveGrid_MSD.py

Region of Attraction estimation
"""

import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt
import _pickle as pkl

from youngknight.frameworks.Shooting.policies import LinearPolicy
from youngknight.frameworks.Shooting.ctg import BasicCTG
from youngknight.models.Dynamics import LinMSD

import params.exp_MSD2 as exp

home = 'results/'
pol_filename = 'Lin_Pol_%s.pkl'%exp.exp_id
ctg_filename = 'Lin_CTG_%s.pkl'%exp.exp_id
Diffs_filename = 'Diffs_Lin_Pol_%s.png'%exp.exp_id
Edges_filename = 'Edges_Lin_Pol_%s.png'%exp.exp_id
Data_filename = 'roa_results.pkl'

min_tau = 0.001
max_tau = 0.01
div = 10.0

def eval_grid(p_grid, v_grid, tau, pol, dyn, ctg, L_g, L_c):
    s = []
    for i in range(p_grid.shape[0]):
        for j in range(v_grid.shape[1]):
            s.append(np.array([p_grid[i, j], v_grid[i, j]]))

    s = np.stack(s, axis=0)
    p = exp.param_gen(s.shape[0])
    a = pol.pol(s, p, 0)
    s_ = dyn.step(s, a, p, 0)
    C = ctg.ctg(s, p, 0)
    C_ = ctg.ctg(s_, p, 0)
    C_diff = C_ - C

    thresh = L_c*(L_g + 1)*tau
    cat = np.zeros(p_grid.shape)

    for i in range(p_grid.shape[0]):
        for j in range(v_grid.shape[1]):
            if C_diff[i*v_grid.shape[1]+j] > thresh:
                cat[i, j] = 1
            elif C_diff[i*v_grid.shape[1]+j] < -thresh:
                cat[i, j] = -1
            elif tau/div >= min_tau:
                tau_new = tau/div
                p_axis_new = np.arange(p_grid[i,j]-tau+tau_new, p_grid[i,j]+tau, 2*tau_new)
                v_axis_new = np.arange(v_grid[i,j]-tau+tau_new, v_grid[i,j]+tau, 2*tau_new)
                p_grid_new, v_grid_new = np.meshgrid(p_axis_new, v_axis_new, indexing='ij')
                cat_new = eval_grid(p_grid_new, v_grid_new, tau_new, pol, dyn, ctg, L_g, L_c)
                if cat_new.min() == cat_new.max():
                    cat[i, j] = cat_new.min()

    return cat

if __name__ == '__main__':
    print('Setting up Models...')
    dyn = LinMSD(params=exp.dyn_params, home=home, name='pend_model_dyn')

    state_size = dyn.state_size
    actin_size = dyn.actin_size
    param_size = dyn.param_size

    pol = LinearPolicy(state_size, actin_size, param_size, load_file=pol_filename, home=home, name='basic_pol')
    ctg = BasicCTG(state_size, param_size, load_file=ctg_filename, home=home, name='basic_ctg')

    print('Setting up grids...')
    p_axis_pos = np.arange(max_tau, exp.p_max_test, 2*max_tau)
    p_axis_neg = -np.flip(np.arange(max_tau, -exp.p_min_test, 2*max_tau))
    p_axis = np.concatenate([p_axis_neg, p_axis_pos])

    v_axis_pos = np.arange(max_tau, exp.p_max_test, 2*max_tau)
    v_axis_neg = -np.flip(np.arange(max_tau, -exp.p_min_test, 2*max_tau))
    v_axis = np.concatenate([v_axis_neg, v_axis_pos])

    print('Running Analysis...')
    p = exp.param_gen(1)
    ctg_weights = ctg.save_weights()
    pol_weights = pol.save_weights()
    A = np.array([[1-exp.dt**2*p[0, 1]/p[0, 0], exp.dt-exp.dt**2*p[0, 2]/p[0, 0]], [-exp.dt*p[0, 1]/p[0, 0], 1-exp.dt*p[0, 2]/p[0, 0]]])

    L_c = np.linalg.norm(ctg_weights['W1'], ord=np.inf)*np.linalg.norm(ctg_weights['W2'], ord=np.inf)
    L_p = np.linalg.norm(pol_weights['K'], ord=np.inf)
    L_f = np.linalg.norm(A, ord=np.inf)
    L_g = L_f*(L_p + 1)

    p_grid, v_grid = np.meshgrid(p_axis, v_axis, indexing='ij')
    c_grid = eval_grid(p_grid, v_grid, max_tau, pol, dyn, ctg, L_g, L_c)

    print('Finding Edges...')
    Kx = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], np.float32)
    Ky = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]], np.float32)
    Ix = ndimage.filters.convolve(c_grid, Kx)
    Iy = ndimage.filters.convolve(c_grid, Ky)

    edges = np.hypot(Ix, Iy)
    edges /= edges.max()

    print('Finding Max V...')
    true_edges = np.zeros(edges.shape)
    V_lim = np.inf

    for i in range(edges.shape[0]):
        for j in range(edges.shape[1]):
            if c_grid[i, j] == -1 and edges[i, j] > 0 and p_grid[i, j]**2 + v_grid[i, j]**2 >= 4.0:
                true_edges[i, j] = 1

                C = ctg.ctg(np.array([[p_grid[i, j], v_grid[i, j]]]), p, 0)
                if C < V_lim:
                    V_lim = C

    print('Calculating sublevel set bounds...')
    theta = np.arange(-np.pi, np.pi, 0.1)
    roa_bound = np.zeros((theta.shape[0], 2))

    for i in range(theta.shape[0]):
        r_bound = [0, 10]
        while r_bound[1] - r_bound[0] > 0.01:
            r_test = 0.5*(r_bound[0] + r_bound[1])
            s_test = np.array([[r_test*np.cos(theta[i]), r_test*np.sin(theta[i])]])
            V_test = ctg.ctg(s_test, p, 0)

            if V_test > V_lim:
                r_bound[1] = r_test
            else:
                r_bound[0] = r_test

        roa_bound[i, 0] = 0.5*(r_bound[0] + r_bound[1])*np.cos(theta[i])
        roa_bound[i, 1] = 0.5*(r_bound[0] + r_bound[1])*np.sin(theta[i])

    roa_bound = np.concatenate([roa_bound, np.expand_dims(roa_bound[0, :], axis=0)], axis=0)

    y_upper = np.array([exp.v_min_test, exp.v_max_test])
    y_lower = np.array([exp.v_min_test, exp.v_max_test])

    x_upper = (exp.f_max - pol_weights['K'][1, 0]*y_upper)/pol_weights['K'][0, 0]
    x_lower = (exp.f_min - pol_weights['K'][1, 0]*y_lower)/pol_weights['K'][0, 0]

    print('Done, Saving Results...')
    pkl.dump([p_grid, v_grid, c_grid, V_lim], open(home+Data_filename, 'wb'))

    print('Generating figures...')
    '''for i in range(c_grid.shape[0]):
        for j in range(c_grid.shape[1]):
            corners_x = [p_grid[i,j]+max_tau, p_grid[i,j]+max_tau, p_grid[i,j]-max_tau, p_grid[i,j]-max_tau]
            corners_y = [v_grid[i,j]+max_tau, v_grid[i,j]-max_tau, v_grid[i,j]-max_tau, v_grid[i,j]+max_tau]
            if c_grid[i,j] == -1:
                color = 'b'
            elif c_grid[i,j] == 1:
                color = 'r'
            else:
                color = 'm'

            plt.fill(corners_x, corners_y, color)'''
    corners_p = np.concatenate([p_axis_neg-max_tau, np.zeros(1), p_axis_pos+max_tau])
    corners_v = np.concatenate([v_axis_neg-max_tau, np.zeros(1), v_axis_pos+max_tau])
    corners_p, corners_v = np.meshgrid(corners_p, corners_v, indexing='ij')
    plt.pcolormesh(corners_p.T, corners_v.T, c_grid.T)
    plt.plot(roa_bound[:, 0], roa_bound[:, 1])
    plt.plot(x_upper, y_upper, 'r')
    plt.plot(x_lower, y_lower, 'r')
    plt.xlabel('position')
    plt.ylabel('velocity')
    plt.savefig(home+Diffs_filename)

    fig = plt.figure()
    plt.pcolormesh(corners_p.T, corners_v.T, true_edges.T)
    plt.xlabel('position')
    plt.ylabel('velocity')
    plt.savefig(home+Edges_filename)

    plt.show()
