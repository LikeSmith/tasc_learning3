"""
Lin_policy_analysis.py

analyse learned policy using montecarlo sampeling to see if points are descendant
"""

import numpy as np
import matplotlib.pyplot as plt

from youngknight.frameworks.Shooting.policies import LinearPolicy
from youngknight.frameworks.Shooting.ctg import BasicCTG
from youngknight.models.Dynamics import LinMSD
from youngknight.core.utils import rand_vectors

import params.exp_MSD2 as exp

home = 'results/'
pol_filename = 'STD_Pol_%s.pkl'%exp.exp_id
ctg_filename = 'STD_CTG_%s.pkl'%exp.exp_id

test_n = 100

if __name__ == '__main__':
    print('Setting up Models...')
    dyn = LinMSD(params=exp.dyn_params, home=home, name='pend_model_dyn')

    state_size = dyn.state_size
    actin_size = dyn.actin_size
    param_size = dyn.param_size

    pol = LinearPolicy(state_size, actin_size, param_size, load_file=pol_filename, home=home, name='basic_pol')
    ctg = BasicCTG(state_size, param_size, load_file=ctg_filename, home=home, name='basic_ctg')

    print('Running simulation...')
    s0 = rand_vectors(test_n, [exp.p_min, exp.v_min], [exp.p_max, exp.v_max])
    p = rand_vectors(test_n, [exp.m_min, exp.k_min, exp.c_min], [exp.m_max, exp.k_max, exp.c_max])

    t = np.arange(exp.n_steps+1)*exp.dt
    s = np.zeros((test_n, exp.n_steps+1)+state_size)
    a = np.zeros((test_n, exp.n_steps)+actin_size)
    C = np.zeros((test_n, exp.n_steps+1))

    s[:, 0, :] = s0
    C[:, 0] = ctg.ctg(s0, p, 0)[:, 0]

    for i in range(exp.n_steps):
        a[:, i, :] = pol.pol(s[:, i, :], p, i)

        s[:, i+1, :] = dyn.step(s[:, i, :], a[:, i, :], p, i+1)
        C[:, i+1] = ctg.ctg(s[:, i+1, :], p, i+1)[:, 0]

    plt.figure()
    plt.plot(t, s[:, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('theta (rad)')

    plt.figure()
    plt.plot(t, s[:, :, 1].T)
    plt.xlabel('time (s)')
    plt.ylabel('omega (rad/s)')

    plt.figure()
    plt.plot(t[:-1], a[:, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('tau (N-m)')

    plt.figure()
    plt.plot(t, C.T)
    plt.xlabel('time (s)')
    plt.ylabel('C')
