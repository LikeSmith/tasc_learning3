"""
LearnPol_CTG.py

Learns a policy and Neural Network cost-to-go function simulataniously using
standard shooting method
"""

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

from youngknight.frameworks.Shooting import ShootingCTG
from youngknight.frameworks.Shooting.policies import BasicPol
from youngknight.frameworks.Shooting.ctg import BasicCTG
from youngknight.models.Dynamics import LinMSD
from youngknight.models.Rewards import LQRReward

import params.exp_MSD1 as exp

home = 'results/'
pol_filename = 'CTG_Pol_%s.pkl'%exp.exp_id
ctg_filename = 'CTG_CTG_%s.pkl'%exp.exp_id

n_epochs = 100
n_batches = 1000
batch_size = 128
val_size = 200

if __name__ == '__main__':
    print('Setting up Models...')
    sess = tf.Session()
    dyn = LinMSD(params=exp.dyn_params, home=home, name='MSD_model_dyn', sess=sess)

    state_size = dyn.state_size
    actin_size = dyn.actin_size
    param_size = dyn.param_size

    rwd = LQRReward(state_size, state_size, actin_size, param_size, params=exp.lqr_params, home=home, name='lqr_reward', sess=sess)
    pol = BasicPol(state_size, actin_size, param_size, params=exp.pol_params, home=home, name='basic_pol', sess=sess)
    ctg = BasicCTG(state_size, param_size, params=exp.ctg_NN_params, home=home, name='basic_ctg', sess=sess)

    print('Building learner...')
    learner = ShootingCTG(dyn, rwd, pol, ctg, params=exp.lrn_params, home=home, name='shooting_learner', sess=sess)

    hist = learner.train(n_epochs, n_batches, batch_size, val_size)

    print('Done!')
    t = np.arange(exp.lrn_params['n_steps']+1)*exp.dt

    plt.figure()
    plt.plot(hist['ave_loss_pol'])
    plt.plot(hist['val_loss_pol'])
    plt.xlabel('epoch')
    plt.ylabel('loss_pol')
    plt.legend(['Training', 'Validation'])

    plt.figure()
    plt.plot(hist['ave_loss_ctg'])
    plt.plot(hist['val_loss_ctg'])
    plt.xlabel('epoch')
    plt.ylabel('loss_val')
    plt.legend(['Training', 'Validation'])

    plt.figure()
    plt.plot(t, hist['s'][hist['min_loss_epoch'], :, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('theta (rad)')

    plt.figure()
    plt.plot(t, hist['s'][hist['min_loss_epoch'], :, :, 1].T)
    plt.xlabel('time (s)')
    plt.ylabel('omega (rad/s)')

    plt.figure()
    plt.plot(t[:-1], hist['a'][hist['min_loss_epoch'], :, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('tau (N-m)')

    plt.figure()
    plt.plot(t, hist['ctg'][hist['min_loss_epoch'], :, :].T)
    plt.xlabel('time (s)')
    plt.ylabel('cost-to-go')

    plt.figure()
    plt.plot(t, hist['ctg_est'][hist['min_loss_epoch'], :, :].T)
    plt.xlabel('time (s)')
    plt.ylabel('cost-to-go estimate')

    print('Saving Results...')
    pol.save(pol_filename)
    ctg.save(ctg_filename)
