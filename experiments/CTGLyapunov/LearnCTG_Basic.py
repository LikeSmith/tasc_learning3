"""
LearnCTG_Basic.py

Learns a policy and Neural Network cost-to-go function independantly using
standard shooting method
"""

import numpy as np
import matplotlib.pyplot as plt

from youngknight.frameworks.Shooting.policies import LinearPolicy
from youngknight.frameworks.Shooting.ctg import BasicCTG
from youngknight.models.Dynamics import LinMSD
from youngknight.models.Rewards import LQRReward

import params.exp_MSD2 as exp

home = 'results/'
pol_filename = 'Lin_Pol_%s.pkl'%exp.exp_id
ctg_filename = 'Lin_CTG_%s.pkl'%exp.exp_id

n_epochs = 100
n_batches = 1000
batch_size = 128
val_size = 200

test_n = 100

if __name__ == '__main__':
    print('Setting up Models...')
    dyn = LinMSD(params=exp.dyn_params, home=home, name='pend_model_dyn')

    state_size = dyn.state_size
    actin_size = dyn.actin_size
    param_size = dyn.param_size

    rwd = LQRReward(state_size, state_size, actin_size, param_size, params=exp.lqr_params, home=home, name='lqr_reward')
    pol = LinearPolicy(state_size, actin_size, param_size, load_file=pol_filename, home=home, name='basic_pol')
    ctg = BasicCTG(state_size, param_size, dyn=dyn, pol=pol, rwd=rwd, params=exp.ctg_NN_params, home=home, name='basic_ctg')

    print('Training CTG...')
    hist_ctg = ctg.train(n_epochs, n_batches, batch_size, val_size)

    print('Done!')
    t = np.arange(exp.lrn_params['n_steps']+1)*exp.dt

    print('Running simulation...')
    s0 = exp.init_state_gen(test_n)
    p = exp.param_gen(test_n)

    t = np.arange(exp.n_steps+1)*exp.dt
    s = np.zeros((test_n, exp.n_steps+1)+state_size)
    a = np.zeros((test_n, exp.n_steps)+actin_size)
    C = np.zeros((test_n, exp.n_steps+1))

    s[:, 0, :] = s0
    C[:, 0] = ctg.ctg(s0, p, 0)[:, 0]

    for i in range(exp.n_steps):
        a[:, i, :] = pol.pol(s[:, i, :], p, i)

        s[:, i+1, :] = dyn.step(s[:, i, :], a[:, i, :], p, i+1)
        C[:, i+1] = ctg.ctg(s[:, i+1, :], p, i+1)[:, 0]

    plt.figure()
    plt.plot(hist_ctg['ave_loss'])
    plt.plot(hist_ctg['val_loss'])
    plt.xlabel('epoch')
    plt.ylabel('loss_val')
    plt.legend(['Training', 'Validation'])

    plt.figure()
    plt.plot(t, s[:, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('theta (rad)')

    plt.figure()
    plt.plot(t, s[:, :, 1].T)
    plt.xlabel('time (s)')
    plt.ylabel('omega (rad/s)')

    plt.figure()
    plt.plot(t[:-1], a[:, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('tau (N-m)')

    plt.figure()
    plt.plot(t, C.T)
    plt.xlabel('time (s)')
    plt.ylabel('C')

    print('Saving Results...')
    ctg.save(ctg_filename)
