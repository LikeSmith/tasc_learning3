"""
TrainSwarmController_SI.py

Experiment to learn tracking control for single integrator swarm
"""

import numpy as np
import matplotlib.pyplot as plt
import _pickle as pkl

from youngknight.sims import SingleIntegratorSwarm
from youngknight.frameworks.a3c.actors import Basic_SwarmActor_Param
from youngknight.frameworks.a3c import A3C
from youngknight.trajectories import Dataset
from youngknight.frameworks.AbstractionEncoder.encoders import HierarchicalEncoder
from youngknight.core import quad

dt = 0.05
L = 1.0
aux_size = 5
home = 'results/'
log_dir = 'logs'
glb_filename = 'global_actor_2_trk_SI.pkl'
loc_filename = 'local_actor%d_2_trk_SI.pkl'
enc_filename = 'Hierarchical_Encoder2_work1.pkl'
dat_filename = 'SampleTrajectories2_work1.pkl'

abst_state_size = 4
swrm_state_size = 2
swrm_param_size = 1

Q_i = 0.0
R_i = 0.01
Q_abs = 1.0*np.eye(abst_state_size)

sim_limits = {}
sim_limits['p_max'] = 2.0
sim_limits['p_min'] = -2.0
sim_limits['v_max'] = 5.0
sim_limits['v_min'] = -5.0
sim_limits['m_max'] = 0.5
sim_limits['m_min'] = 0.25

actor_params = {}
actor_params['swarm_size'] = 10
#actor_params['max_a'] = [1.0]
#actor_params['min_a'] = [-1.0]
actor_params['des_traj'] = Dataset(pkl.load(open(home+dat_filename, 'rb'))['actions'])
actor_params['hidden_size_policy'] = 200
actor_params['hidden_size_value'] = 100
actor_params['hidden_size_aux'] = 200
actor_params['activ_policy'] = 'leakyrelu_0.1'
actor_params['activ_value'] = 'leakyrelu_0.1'
actor_params['activ_aux'] = 'leakyrelu_0.1'
actor_params['use_bias_pol'] = True
actor_params['use_bias_val'] = True
actor_params['use_bias_aux'] = True
actor_params['use_l1_loss_pol'] = False
actor_params['use_l2_loss_pol'] = False
actor_params['use_l1_loss_val'] = False
actor_params['use_l2_loss_val'] = True
actor_params['reg_param'] = 1e-2
actor_params['zero_reg'] = 0
actor_params['beta_entropy'] = 0.01
actor_params['sig_min'] = 1e-4
actor_params['T_max'] = 2000
actor_params['t_max'] = 200
actor_params['pol_learning_rate'] = 0.0001
actor_params['val_learning_rate'] = 0.001
actor_params['global_update_rate'] = 10
actor_params['validation_rate'] = 10
actor_params['gamma'] = 0.9
actor_params['use_noise'] = False
actor_params['regen_mode'] = 2
actor_params['regen_rate'] = 50
actor_params['verbosity'] = 2
actor_params['dirs'] = 0

enc = HierarchicalEncoder(abst_state_size, swrm_state_size, swrm_param_size, load_file=enc_filename, home=home, name='Encoder')

def calc_abs_state(state, k, params):
   return enc.encode(state, params)
    
def calc_aux_state(state, k, params):
    return np.zeros((state.shape[0], aux_size))

def glb_rwd(abs_state, abs_state1, action, k, params, des_traj):
    abs_err = abs_state - des_traj(k)
    cost = quad(abs_err, Q_abs)
    
    return -cost

def rwd(state, state1, action, k, params, des_traj):
    p = state[:, 0]
    v = action[:, 0]
    
    cost = Q_i*p**2 + R_i*v**2
    
    return -cost/actor_params['swarm_size']

if __name__ == '__main__':
    print('setting up simulator...')
    sim = SingleIntegratorSwarm(limits=sim_limits, del_t=dt, L=L, enforce_limits=True, full_state_feedback=True, rwd=rwd, glb_rwd=glb_rwd, abs_calc=calc_abs_state, aux_calc=calc_aux_state, name='SwarmAlone_trk_SI')
    actor_params['sim_params'] = sim.param_gen(1, actor_params['swarm_size'])
    
    print('initializing_framework...')
    learner = A3C(sim, Basic_SwarmActor_Param, actor_params, use_gym=False, use_obs=False, home=home, name='SwarmAlone_reg_SI_A3C', log_dir=log_dir)
    
    #learner.actors[0].train()
    print('starting_actors...')
    learner.start_actors(with_val=True)
    learner.join(use_best=True)
    
    print('training finished!')
    learner.save_results(glb_filename)
    learner.save_actors(loc_filename)
    
    hists = learner.get_histories()
    glb_hist = learner.get_val_history()
    
    pkl.dump([hists, glb_hist, learner.get_glb_r(), actor_params['sim_params']], open(home+'training_reg_SI_results.pkl', 'bw'))
    
    plt.figure()
    plt.plot(learner.get_glb_r())
    plt.plot(glb_hist['ep_stamp'], glb_hist['running_ep_r'])
    plt.xlabel('global episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['running_ep_r'])))
    plt.xlabel('local episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_p'])))
    plt.xlabel('update number')
    plt.ylabel('policy loss')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_v'])))
    plt.xlabel('update number')
    plt.ylabel('value loss')
    
    print('Generating test trajectory...')
    ctrl = lambda state, k, params, abs_state, aux_state, des_traj: learner.get_final_actor().policy(state, k, int(params[0, 2]), abs_state, aux_state, des_traj)
    aux = lambda state, k, params: learner.get_final_actor().aux_state(state[:, :, 0:1], k)
    
    sim = SingleIntegratorSwarm(limits=sim_limits, del_t=dt, L=L, enforce_limits=True, full_state_feedback=False, rwd=rwd, glb_rwd=glb_rwd, abs_calc=calc_abs_state, aux_calc=aux, ctrl=ctrl, name='SwarmAlone_reg_SI_test')
    
    x_0 = sim.init_state_gen(1, actor_params['swarm_size'])
    p = sim.param_gen(1, actor_params['swarm_size'])
    
    n = 200
    res, _ = sim.gen_trajectories(x_0, n, params=p, verbosity=1, des_traj=actor_params['des_traj'])
    t = np.arange(n+1)*dt
    values = np.zeros(n+1)
    abs_states_des = np.zeros((n+1, actor_params['des_traj'].channels*(actor_params['des_traj'].dirs + 1)))
    
    for i in range(n+1):
        abs_states_des[i, :] = actor_params['des_traj'](i)
        values[i] = learner.get_final_actor().value(res['states'][:, i], i, actor_params['des_traj'])
        
    plt.figure()
    plt.plot(t, res['states'][0, :, :, 0])
    plt.title('Test Run: Position')
    
    plt.figure()
    plt.plot(t[:-1], res['actions'][0, :, :, 0])
    plt.title('Test Run: Velocity')
    
    plt.figure()
    plt.plot(t, abs_states_des[:, 0::(actor_params['des_traj'].dirs+1)], ':')
    plt.plot(t, res['abs_states'][0, :, :])
    plt.title('Abstract State')
    
    plt.figure()
    plt.plot(t, res['aux_states'][0, :, :])
    plt.title('Auxilliary State')
    
    plt.figure()
    plt.plot(t[:-1], res['rewards'][0, :])
    plt.title('Rewards')
    
    plt.figure()
    plt.plot(t, values)
    plt.title('Values')
    
    plt.show()