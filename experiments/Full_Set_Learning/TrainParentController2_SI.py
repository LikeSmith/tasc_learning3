'''
TrainParentController_SI.py

Experiment to use A3C to learn policy for learned abstract dynamics, outside of the 
swarm parent system
'''

import numpy as np
import matplotlib.pyplot as plt
import _pickle as pkl

from youngknight.sims import ExtSim
from youngknight.frameworks.AbstractionEncoder.dynamics import DoubleIntegratorDynamics
from youngknight.frameworks.a3c.actors import Basic_Actor_Cont
from youngknight.frameworks.a3c import A3C
from youngknight.core import quad

max_swarm_size = 10
min_swarm_size = 2
abst_state_size = 4
prnt_state_size = 2
chld_state_size = 2
dt = 0.05
home = 'results/'
glb_filename = 'global_actor2.pkl'
loc_filename = 'local_actor%d.pkl'
dyn_filename = 'Hierarchical_Dynamics2.pkl'
n_save_traj = 1000
t_f_traj = 10.0
traj_filename = 'SampleTrajectories2.pkl'

Q = np.diag([1.0, 0.1])
R = 0.001*np.eye(4)

theta_min = -0.2
theta_max = 0.2
omega_min = -0.1
omega_max = 0.1

sim_limits = {}
sim_limits['a_min'] = np.array([-10.0, -10.0, -10, -10.0])
sim_limits['a_max'] = np.array([10.0, 10.0, 10.0, 10.0])
sim_limits['s_min'] = np.array([theta_min, omega_min])
sim_limits['s_max'] = np.array([theta_max, omega_max])
sim_limits['p_min'] = np.array([0])
sim_limits['p_max'] = np.array([1])

sim_noise_params = {}
sim_noise_params['process_mean'] = np.zeros((1, 2))
sim_noise_params['process_cov'] = np.eye(2)*0.0001
sim_noise_params['signal_mean'] = np.zeros((1, 1))
sim_noise_params['signal_cov'] = np.eye(1)*0.0001

actor_params = {}
actor_params['hidden_size_policy'] = 200
actor_params['hidden_size_value'] = 200
actor_params['activ_policy'] = 'leakyrelu_0.1'
actor_params['activ_value'] = 'leakyrelu_0.1'
actor_params['min_a'] = sim_limits['a_min']
actor_params['max_a'] = sim_limits['a_max']
actor_params['pol_learning_rate'] = 0.0001
actor_params['val_learning_rate'] = 0.001
actor_params['zero_reg'] = 0
actor_params['use_l1_loss'] = False
actor_params['use_l2_loss'] = True
actor_params['use_bias'] = True
actor_params['T_max'] = 10000
actor_params['t_max'] = 200
actor_params['global_update_rate'] = 10
actor_params['validation_rate'] = 10
actor_params['use_es'] = False
actor_params['gamma'] = 0.9
actor_params['beta_entropy'] = 0.01
actor_params['sig_min'] = 1e-4
actor_params['use_noise'] = False
actor_params['verbosity'] = 2

def rwd(state, state1, action, k, params):
    cost = quad(state, Q) + quad(action, R)
    return -cost

if __name__ == '__main__':
    print('setting up simulator...')
    abs_dyn = DoubleIntegratorDynamics(abst_state_size, prnt_state_size, load_file=dyn_filename, home=home, name='dynamics')
    step_fun = lambda state, action, k, params: abs_dyn.step(state, action)
    sim = ExtSim(step_fun, sim_limits, sim_noise_params, dt=dt, enforce_limits=False, rwd=rwd, name='AbsDynSim')
    actor_params['sim_params'] = sim.param_gen(1)
    
    print('initializing_framework...')
    learner = A3C(sim, Basic_Actor_Cont, actor_params, use_gym=False, use_obs=False, home=home, name='PlaneAlone_A3C')
    
    print('starting_actors...')
    learner.start_actors(with_val=True)
    learner.join(use_best=True)
    
    learner.save_results(glb_filename)
    learner.save_actors(loc_filename)
    
    print('training finished!')
    hists = learner.get_histories()
    glb_hist = learner.get_val_history()
    
    pkl.dump([hists, glb_hist, learner.get_glb_r(), actor_params['sim_params']], open(home+'training_results.pkl', 'bw'))
    
    plt.figure()
    plt.plot(learner.get_glb_r())
    plt.plot(glb_hist['ep_stamp'], glb_hist['running_ep_r'])
    plt.xlabel('global episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['running_ep_r'])))
    plt.xlabel('local episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_p'])))
    plt.xlabel('update number')
    plt.ylabel('policy loss')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_v'])))
    plt.xlabel('update number')
    plt.ylabel('value loss')
    
    ctrl = lambda state, k, params, des_traj: learner.get_final_actor().policy(state, k)
    sim = ExtSim(step_fun, sim_limits, sim_noise_params, dt=dt, enforce_limits=True, full_state_feedback=True, ctrl=ctrl, rwd=rwd, name='Plane')
    
    x_0 = sim.init_state_gen(10)
    x_0[0, :] = np.zeros(2)
    
    print('running simulation...')
    n = 200
    res, _ = sim.gen_trajectories(x_0, n, params=actor_params['sim_params'], verbosity=1)
    t = np.arange(n+1)*dt
    
    plt.figure()
    plt.plot(t.T, res['states'][:, :, 0].T)
    plt.title('Test Run: Position')
    
    plt.figure()
    plt.plot(t.T, res['states'][:, :, 1].T)
    plt.title('Test Run: Velocity')
    
    plt.figure()
    plt.plot(t[:-1].T, res['actions'][:, :, 0].T)
    plt.title('Abs State 1')
    
    plt.figure()
    plt.plot(t[:-1].T, res['actions'][:, :, 1].T)
    plt.title('Abs State 2')
    
    plt.figure()
    plt.plot(t[:-1].T, res['actions'][:, :, 2].T)
    plt.title('Abs State 3')
    
    plt.figure()
    plt.plot(t[:-1].T, res['actions'][:, :, 3].T)
    plt.title('Abs State 4')
    
    plt.figure()
    plt.plot(t[:-1].T, res['rewards'].T)
    
    plt.show()
    
    print('calculating trajectories for swarm to train on')
    x_0 = sim.init_state_gen(n_save_traj)
    res, _ = sim.gen_trajectories(x_0, int(t_f_traj/dt)+1, params=actor_params['sim_params'], verbosity=1)
    
    pkl.dump(res, open(home+traj_filename, 'wb'))
    
    print('done')