"""
TASC_Learner.py
"""

import tensorflow as tf
import numpy as np
from tqdm import trange
import math
import _pickle as pkl
import os
from youngknight.core import rand_vectors

SESS = tf.Session()

DT = 0.05
N = 100

PRT_STATE_SIZE = 2
SWM_STATE_SIZE = 2
SWM_ACTIN_SIZE = 1
SWM_SIZE = 8

Q_PRT = np.array([[10.0, 0.0], [0.0, 1.0]])
Q_SWM = np.array([[1.0, 0.0], [0.0, 1.0]])

ABS_STATE_SIZE = 4
AUX_STATE_SIZE = 5

ABS_ENC_H_SIZE = 100
ABS_DYN_H_SIZE = 100
AUX_ENC_H_SIZE = 100
PRT_POL_H_SIZE = 100
SWM_POL_H_SIZE = 100

LR = 0.001

N_EPOCH = 100
N_BATCH = 1000
BAT_SIZE = 128
VAL_SIZE = 512

PATIENCE = 10

HOME = 'results/'
LOGS = 'logs/'
AUTOSAVE_FILENAME = 'autosave_epoch%d.pkl'
VAL_RSLT_FILENAME = 'val_data_epoch%d.pkl'
FINAL_FILENAME = 'final_solution.pkl'

class Abs_Enc(object):
    def __init__(self):
        self.h_layer = tf.keras.layers.Dense(ABS_ENC_H_SIZE, tf.nn.leaky_relu)
        self.o_layer = tf.keras.layers.Dense(ABS_STATE_SIZE)
        self.scope = None

    def __call__(self, s_s):
        with tf.variable_scope('AbsEnc' if self.scope is None else self.scope) as scope:
            h = self.h_layer(tf.concat([s_s[:, i, :] for i in range(SWM_SIZE)], axis=1))
            o = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return o

class Aux_Enc(object):
    def __init__(self):
        self.h_layer = tf.keras.layers.Dense(AUX_ENC_H_SIZE, tf.nn.leaky_relu)
        self.o_layer = tf.keras.layers.Dense(AUX_STATE_SIZE)
        self.scope = None

    def __call__(self, s_s):
        with tf.variable_scope('AuxEnc' if self.scope is None else self.scope) as scope:
            h = self.h_layer(tf.concat([s_s[:, i, :] for i in range(SWM_SIZE)], axis=1))
            o = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return o

class Abs_Dyn(object):
    def __init__(self):
        self.h_layer = tf.keras.layers.Dense(ABS_DYN_H_SIZE, tf.nn.leaky_relu)
        self.o_layer = tf.keras.layers.Dense(PRT_STATE_SIZE)
        self.scope = None

    def __call__(self, s_p, s_a):
        with tf.variable_scope('AbsDyn' if self.scope is None else self.scope) as scope:
            comb_in = tf.concat((s_p, s_a), axis=1)
            h = self.h_layer(comb_in)
            o = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return o

class Prt_Pol(object):
    def __init__(self):
        self.h_layer = tf.keras.layers.Dense(PRT_POL_H_SIZE, tf.nn.leaky_relu)
        self.o_layer = tf.keras.layers.Dense(ABS_STATE_SIZE)
        self.scope = None

    def __call__(self, s_p):
        with tf.variable_scope('PrtPol' if self.scope is None else self.scope) as scope:
            h = self.h_layer(s_p)
            o = self.o_layer(h)
            if self.scope is None:
                self.scope = scope
        return o
class Swm_Pol(object):
    def __init__(self):
        self.h_layer = tf.keras.layers.Dense(SWM_POL_H_SIZE, tf.nn.leaky_relu)
        self.o_layer = tf.keras.layers.Dense(SWM_ACTIN_SIZE)
        self.scope = None

    def __call__(self, s_s, s_a, s_d, s_x):
        with tf.variable_scope('SwmPol' if self.scope is None else self.scope) as scope:
            a_s = []
            for i in range(SWM_SIZE):
                comb_in = tf.concat((s_s[:, i, :], s_a, s_d, s_x), axis=1)
                h = self.h_layer(comb_in)
                a_s.append(self.o_layer(h))
            a_s = tf.stack(a_s, axis=1)
            if self.scope is None:
                self.scope = scope
        return a_s

class System(object):
    theta_min = -np.pi/60.0
    theta_max = np.pi/60.0
    omega_min = -1.0
    omega_max = 1.0
    p_min = -2.0
    p_max = 2.0
    v_min = -1.0
    v_max = 1.0
    m_min = 1.5
    m_max = 2.0
    J = 1.5
    gamma_1 = 0.01
    gamma_2 = 1000
    gamma_3 = 700
    gamma_4 = 0.07
    gamma_5 = 1000
    gamma_6 = 0.5
    g = 9.81
    c = 1.5

    def __init__(self):
        with tf.variable_scope('DynSystem') as scope:
            self.swm_m = tf.constant(rand_vectors(SWM_SIZE, np.array([self.m_min]), np.array([self.m_max])), dtype='float')[0, :]
            self.J_tf = tf.constant(self.J, dtype='float')
            self.gamma_1_tf = tf.constant(self.gamma_1, dtype='float')
            self.gamma_2_tf = tf.constant(self.gamma_2, dtype='float')
            self.gamma_3_tf = tf.constant(self.gamma_3, dtype='float')
            self.gamma_4_tf = tf.constant(self.gamma_4, dtype='float')
            self.gamma_5_tf = tf.constant(self.gamma_5, dtype='float')
            self.gamma_6_tf = tf.constant(self.gamma_6, dtype='float')
            self.g_tf = tf.constant(self.g, dtype='float')
            self.c_tf = tf.constant(self.c, dtype='float')
            self.scope = scope

    def gen_prt_states(self, n):
        return rand_vectors(n, np.array([self.theta_min, self.omega_min]), np.array([self.theta_max, self.omega_max]))

    def gen_swm_states(self, n):
        return rand_vectors((n, SWM_SIZE), np.array([self.p_min, self.v_min]), np.array([self.p_max, self.v_max]))

    def strbk_friction(self, v):
        return self.gamma_1_tf*(tf.tanh(self.gamma_2_tf*v) - tf.tanh(self.gamma_3_tf)*v) + self.gamma_4_tf*tf.tanh(self.gamma_5_tf*v) + self.gamma_6_tf*v

    def prt_dyn_cont(self, s_p, s_s):
        J_eff = self.J_tf + tf.reduce_sum(self.swm_m*(s_s[:, :, 0]**2), axis=1)
        tau = tf.reduce_sum(self.swm_m*s_s[:, :, 0], axis=1)
        J_dot = tf.reduce_sum(2*self.swm_m*(s_s[:, :, 0]*s_s[:, :, 1]), axis=1)
        theta_dot = s_p[:, 1]
        omega_dot = (-self.g_tf*tf.cos(s_p[:, 0])*tau - s_p[:, 1]*J_dot - self.strbk_friction(s_p[:, 1]))/J_eff
        return tf.stack((theta_dot, omega_dot), axis=1)

    def swm_dyn_cont(self, s_s, a_s):
        p_dot = s_s[:, :, 1]
        v_dot = (a_s[:, :, 0] - self.c_tf*s_s[:, :, 1])/self.swm_m
        return tf.stack((p_dot, v_dot), axis=2)

    def dyn(self, s_p_old, s_s_old, a_s_old):
        with tf.variable_scope(self.scope):
            dt = tf.constant(DT, 'float')
            s_p_dot1 = self.prt_dyn_cont(s_p_old, s_s_old)
            s_s_dot1 = self.swm_dyn_cont(s_s_old, a_s_old)
            s_p_dot2 = self.prt_dyn_cont(s_p_old+0.5*dt*s_p_dot1, s_s_old+0.5*dt*s_s_dot1)
            s_s_dot2 = self.swm_dyn_cont(s_s_old+0.5*dt*s_s_dot1, a_s_old)
            s_p_dot3 = self.prt_dyn_cont(s_p_old+0.5*dt*s_p_dot2, s_s_old+0.5*dt*s_s_dot2)
            s_s_dot3 = self.swm_dyn_cont(s_s_old+0.5*dt*s_s_dot2, a_s_old)
            s_p_dot4 = self.prt_dyn_cont(s_p_old+dt*s_p_dot3, s_s_old+dt*s_s_dot3)
            s_s_dot4 = self.swm_dyn_cont(s_s_old+dt*s_s_dot3, a_s_old)

            s_p_new = s_p_old + dt/6.0*(s_p_dot1 + 2*s_p_dot2 + 2*s_p_dot3 + s_p_dot4)
            s_s_new = s_s_old + dt/6.0*(s_s_dot1 + 2*s_s_dot2 + 2*s_s_dot3 + s_s_dot4)

        return s_p_new, s_s_new


class Iteration(object):
    def __init__(self, abs_enc, aux_enc, abs_dyn, prt_pol, swm_pol, sys):
        self.abs_enc = abs_enc
        self.aux_enc = aux_enc
        self.abs_dyn = abs_dyn
        self.prt_pol = prt_pol
        self.swm_pol = swm_pol
        self.sys = sys

    def __call__(self, s_p_old, s_s_old):
        with tf.variable_scope('Iter'):
            s_a_old = self.abs_enc(s_s_old)
            s_x_old = self.aux_enc(s_s_old)

            s_d_old = self.prt_pol(s_p_old)
            a_s_old = self.swm_pol(s_s_old, s_a_old, s_d_old, s_x_old)

            s_p_new, s_s_new = self.sys.dyn(s_p_old, s_s_old, a_s_old)

            s_p_prd = self.abs_dyn(s_p_old, s_a_old)

        return s_p_new, s_s_new, s_a_old, s_x_old, s_d_old, a_s_old, s_p_prd

class TASC_trainer(object):
    def __init__(self):
        self.abs_enc = Abs_Enc()
        self.aux_enc = Aux_Enc()
        self.abs_dyn = Abs_Dyn()
        self.prt_pol = Prt_Pol()
        self.swm_pol = Swm_Pol()
        self.sys = System()
        self.iter = Iteration(self.abs_enc, self.aux_enc, self.abs_dyn, self.prt_pol, self.swm_pol, self.sys)
        self.loss_scope = None
        self.build()

    def build(self):
        with tf.variable_scope('TASC_Learner') as scope:
            self.scope = scope
            self.s_p_0 = tf.placeholder('float', (None, PRT_STATE_SIZE), 's_p_0')
            self.s_s_0 = tf.placeholder('float', (None, SWM_SIZE, SWM_STATE_SIZE), 's_s_0')

            Q_p = tf.constant(Q_PRT, dtype='float')
            Q_s = tf.constant(Q_SWM, dtype='float')

            s_p = [self.s_p_0]
            s_s = [self.s_s_0]
            s_a = []
            s_x = []
            s_d = []
            a_s = []
            l_prt = []
            l_swm = []
            l_dyn = []
            l_abs = []

            for i in range(N):
                s_p_nxt, s_s_nxt, s_a_cur, s_x_cur, s_d_cur, a_s_cur, s_p_prd = self.iter(s_p[i], s_s[i])
                s_p.append(s_p_nxt)
                s_s.append(s_s_nxt)
                s_a.append(s_a_cur)
                s_x.append(s_x_cur)
                s_d.append(s_d_cur)
                a_s.append(a_s_cur)

                with tf.variable_scope('loss' if self.loss_scope is None else self.loss_scope) as scope:
                    l_prt.append(tf.einsum('ij,jk,ik->i',s_p[-1], Q_p, s_p[-1]))
                    l_swm.append(tf.einsum('ijk,kl,ijl->i', s_s[-1], Q_s, s_s[-1]))
                    l_dyn.append(tf.einsum('ij,ij->i', s_p_nxt-s_p_prd, s_p_nxt-s_p_prd))
                    l_abs.append(tf.einsum('ij,ij->i', s_d_cur-s_a_cur, s_d_cur-s_a_cur))

                    if self.loss_scope is None:
                        self.loss_scope = scope

            with tf.variable_scope('outputs'):
                self.outputs = {}
                self.outputs['s_p'] = tf.stack(s_p, axis=1)
                self.outputs['s_s'] = tf.stack(s_s, axis=1)
                self.outputs['s_a'] = tf.stack(s_a, axis=1)
                self.outputs['s_x'] = tf.stack(s_x, axis=1)
                self.outputs['s_d'] = tf.stack(s_d, axis=1)
                self.outputs['a_s'] = tf.stack(a_s, axis=1)

            with tf.variable_scope(self.loss_scope):
                self.outputs['l_prt'] = tf.stack(l_prt, axis=1)
                self.outputs['l_swm'] = tf.stack(l_swm, axis=1)
                self.outputs['l_dyn'] = tf.stack(l_dyn, axis=1)
                self.outputs['l_abs'] = tf.stack(l_abs, axis=1)

                self.losses = {}
                self.losses['L_prt'] = tf.reduce_mean(tf.reduce_sum(self.outputs['l_prt'], axis=1))
                self.losses['L_swm'] = tf.reduce_mean(tf.reduce_sum(self.outputs['l_swm'], axis=1))
                self.losses['L_dyn'] = tf.reduce_mean(tf.reduce_sum(self.outputs['l_dyn'], axis=1))
                self.losses['L_abs'] = tf.reduce_mean(tf.reduce_sum(self.outputs['l_abs'], axis=1))

                total_loss = self.losses['L_prt'] + self.losses['L_swm'] + self.losses['L_dyn'] + self.losses['L_abs']

            with tf.variable_scope('train'):
                self.trainer = tf.train.AdamOptimizer(learning_rate=LR)
                self.train_op = self.trainer.minimize(total_loss)

    def train(self):
        hist = {}

        SESS.run(tf.global_variables_initializer())

        for key in self.losses.keys():
            hist['ave_%s'%key] = np.zeros(N_EPOCH)
            hist['val_%s'%key] = np.zeros(N_EPOCH)

        min_weights=[]
        min_loss = math.inf
        min_loss_epoch = -1
        min_count = 0
        var_list = tf.trainable_variables()

        s_p_0_val = self.sys.gen_prt_states(VAL_SIZE)
        s_s_0_val = self.sys.gen_swm_states(VAL_SIZE)
        feed_dict_val = {self.s_p_0:s_p_0_val, self.s_s_0:s_s_0_val}

        print('Training...')
        for epoch in range(N_EPOCH):
            print('Epoch: %d/%d'%(epoch+1, N_EPOCH))
            batches = trange(N_BATCH)
            for batch in batches:
                s_p_0_bat = self.sys.gen_prt_states(VAL_SIZE)
                s_s_0_bat = self.sys.gen_swm_states(VAL_SIZE)
                feed_dict_bat = {self.s_p_0:s_p_0_bat, self.s_s_0:s_s_0_bat}

                losses, _ = SESS.run([self.losses, self.train_op], feed_dict=feed_dict_bat)

                msg=''
                for key in self.losses.keys():
                    hist['ave_%s'%key][epoch] += losses[key]
                    msg += '(ave_%s: %f)'%(key, hist['ave_%s'%key][epoch]/(batch+1))

                batches.set_description(msg)

            val_losses, val_outputs = SESS.run([self.losses, self.outputs], feed_dict=feed_dict_val)

            total_loss = 0
            msg = ''
            for key in self.losses.keys():
                hist['ave_%s'%key][epoch] /= N_BATCH
                hist['val_%s'%key][epoch] = val_losses[key]
                total_loss += val_losses[key]
                msg += '(%s Ave: %f, Val: %f)'%(key, hist['ave_%s'%key][epoch], hist['val_%s'%key][epoch])

            print('Epoch %d Complete! %s'%(epoch+1, msg))

            if total_loss < min_loss:
                min_loss = total_loss
                min_weights = SESS.run(var_list)
                min_count = 0
                min_loss_epoch = epoch
                if AUTOSAVE_FILENAME is not None:
                    self.save(AUTOSAVE_FILENAME%epoch, to_save=(var_list, min_weights))
                if VAL_RSLT_FILENAME is not None:
                    pkl.dump((val_outputs, val_losses, s_p_0_val, s_s_0_val), open(HOME+VAL_RSLT_FILENAME%epoch, 'wb'))
            else:
                min_count += 1

            if min_count == PATIENCE:
                print('Stopping Training, validation loss has stopped decreasing')
                for key in self.loss_ops.keys():
                    hist['ave_%s'%key] = hist['ave_%s'%key][:epoch+1]
                    hist['val_%s'%key] = hist['val_%s'%key][:epoch+1]
                break

        SESS.run([var.assign(val) for var, val in zip(var_list, min_weights)])
        hist['min_loss_epoch'] = min_loss_epoch

    def save(self, filename, to_save=None):
        if to_save is None:
            var_list = tf.trainable_variables()
            val_list = SESS.run(var_list)
        else:
            var_list, val_list = to_save

        os.makedirs(HOME, exist_ok=True)
        pkl.dump({var.name:val for var, val in zip(var_list, val_list)}, open(HOME+filename, 'wb'))

    def load(self, filename):
        var_list = tf.trainable_variables()
        vals = pkl.load(open(HOME+filename, 'rb'))

        SESS.run([var.assign(vals[var.name]) for var in var_list])

if __name__ == '__main__':
    os.makedirs(HOME, exist_ok=True)
    os.makedirs(LOGS, exist_ok=True)
    print('Setting up networks...')
    learner = TASC_trainer()

    print('Logging...')
    tf.summary.FileWriter(LOGS, SESS.graph)

    print('Training...')
    hist = learner.train()

    print('Saving results')
    learner.save(FINAL_FILENAME)
