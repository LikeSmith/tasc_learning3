"""
SwarmPolicyTest.py

test of the learned policy
"""

import numpy as np
import matplotlib.pyplot as plt
from tqdm import trange

from youngknight.models.Dynamics import MFRMFP_Swarm_SI
from youngknight.frameworks.AbstractionEncoder.encoders import BasicEncoder
#from youngknight.frameworks.AbstractionEncoder.encoders.constant_encoders import MassDistEncoder
from youngknight.frameworks.Shooting.policies import JacobianPol

import params.exp1_SI as exp

f_range = [0.0, 2.0]
phi_range = [0.0, 180.0]
A_range = [0.0, 5.0]
off_range = [-0.0, 0.0]
n_ders = exp.abst_traj_n_ders

if __name__ == '__main__':
    print('Setting up networks...')
    dyn = MFRMFP_Swarm_SI(exp.swrm_dyn_params, home=exp.home, name='swrm_dyn')
    abs_enc = BasicEncoder(exp.abst_state_size, exp.chld_state_size, exp.chld_param_size, load_file=exp.enc_filename, home=exp.home, name='abs_enc')
    #abs_enc = MassDistEncoder(2, exp.chld_state_size, exp.chld_param_size, load_file=exp.enc_filename, home=exp.home, name='abs_enc')
    pol = JacobianPol((exp.max_swarm_size, exp.chld_state_size), (exp.max_swarm_size, exp.chld_actin_size), abs_enc.abs_size, (exp.max_swarm_size, exp.chld_param_size), load_file=exp.swm_pol_filename, home=exp.home, name='swrm_pol')

    print('Setting up sim...')
    s0 = exp.swrm_state_gen(1, 16)
    p = exp.swrm_param_gen(1, 16)

    t = np.arange(0.0, exp.t_f+exp.dt, exp.dt)
    n = t.shape[0]

    s_s = np.zeros((n, 16, 2))
    s_a = np.zeros((n, 4))
    s_s[0, :, :] = s0[0]
    s_a[0, :] = abs_enc.encode(s0, p)[0][0, :]
    s_d = np.zeros((n, n_ders+1, exp.abst_state_size))

    f = f_range[0] + (f_range[1] - f_range[0])*np.random.rand(exp.abst_state_size)
    phi = phi_range[0] + (phi_range[1] - phi_range[0])*np.random.rand(exp.abst_state_size)
    A = A_range[0] + (A_range[1] - A_range[0])*np.random.rand(exp.abst_state_size)
    off = off_range[0] + (off_range[1] - off_range[0])*np.random.rand(exp.abst_state_size)


    for k in trange(n-1):
        '''s_d[k, 0, :] = A*np.sin(f*2*np.pi*t[k] + phi) + off
        sig = 1.0
        for i in range(n_ders):
            if i%2 == 0:
                s_d[k, i+1, :] = sig*A*(f*2*np.pi)**(i+1)*np.cos(f*2*np.pi*t[k] + phi)
                sig *= -1
            else:
                s_d[k, i+1, :] = sig*A*(f*2*np.pi)**(i+1)*np.sin(f*2*np.pi*t[k] + phi)'''
        s_cur = np.expand_dims(s_s[k, :, :], axis=0)
        s_abs = np.expand_dims(s_a[k, :], axis=0)
        a = pol.pol(s_cur, s_abs , np.expand_dims(s_d[k, :, :], axis=0), p, k)
        s_nxt = dyn.step(s_cur, a, p, k)
        s_abs_nxt = abs_enc.encode(s_nxt, p)[0]

        s_s[k+1, :, :] = s_nxt[0, :, :]
        s_a[k+1, :] = s_abs_nxt[0, :]

    plt.figure()
    plt.plot(t, s_s[:, :, 0])
    plt.xlabel('time')
    plt.ylabel('pos')

    plt.figure()
    plt.plot(t, s_s[:, :, 1])
    plt.xlabel('time')
    plt.ylabel('vel')

    for i in range(4):
        plt.figure()
        plt.plot(t, s_a[:, i], 'b')
        plt.plot(t, s_d[:, 0, i], 'b--')
        plt.xlabel('time')
        plt.ylabel('s_a_%d'%i)
