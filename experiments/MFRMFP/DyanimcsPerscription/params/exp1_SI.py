"""
experiment_parameters.py

generates and saves experimental parameters
"""

import numpy as np
import tensorflow as tf

from youngknight.core import rand_vectors

exp_id = 'exp1_SI'
home = 'results/'
log_dir = 'log/'

enc_filename = 'Hierarchical_Encoder_%s.pkl'%exp_id
dyn_filename = 'Hierarchical_Dynamics_%s.pkl'%exp_id
prt_pol_filename = 'Prnt_Pol_%s.pkl'%exp_id
swm_pol_filename = 'Swrm_Pol_%s.pkl'%exp_id

dt = 0.05
t_f = 5.0
verbosity = 2
use_best = True
no_regen = False
patience = 10
abst_traj_n_ders = 1

max_swarm_size = 16
prnt_state_size = 2
prnt_param_size = 8
abst_state_size = 4
chld_state_size = 2
chld_actin_size = 1
chld_param_size = 1

use_lr_search = False
lr_patience = 10
lr_min = 1e-6
lr_max = 10
lr_range = 200

K_abs = np.eye(abst_state_size)*1.0

J = 1.5
gamma_1 = 0.01
gamma_2 = 1000
gamma_3 = 700
gamma_4 = 0.07
gamma_5 = 1000
gamma_6 = 0.5
g = 9.81

J_min = J
J_max = J
gamma_1_min = gamma_1
gamma_1_max = gamma_1
gamma_2_min = gamma_2
gamma_2_max = gamma_2
gamma_23_min = gamma_2 - gamma_3
gamma_23_max = gamma_2 - gamma_3
gamma_41_min = gamma_4 - gamma_1
gamma_41_max = gamma_4 - gamma_1
gamma_5_min = gamma_5
gamma_5_max = gamma_5
gamma_6_min = gamma_6
gamma_6_max = gamma_6
g_min = g
g_max = g

theta_min = -np.pi/60.0
theta_max = np.pi/60.0
omega_min = -1.0
omega_max = 1.0

p_min = -2.0
p_max = 2.0
v_min = -1.0
v_max = 1.0
m_min = 1.5
m_max = 2.0

a_limit = 10.0
a_dot_limit = [5.0]

swrm_state_gen = lambda n,s: rand_vectors((n, s), np.array([p_min, v_min]), np.array([p_max, v_max]))
swrm_param_gen = lambda n,s: rand_vectors((n, s), np.array([m_min]), np.array([m_max]))
prnt_state_gen = lambda n: rand_vectors(n, np.array([theta_min, omega_min]), np.array([theta_max, omega_max]))
prnt_param_gen = lambda n: rand_vectors(n, np.array([J_min, gamma_1_min, gamma_2_min, gamma_23_min, gamma_41_min, gamma_5_min, gamma_6_min, g_min]), np.array([J_max, gamma_1_max, gamma_2_max, gamma_23_max, gamma_41_max, gamma_5_max, gamma_6_max, g_max]))

def abst_state_gen(n, n_ders=None):
    if n_ders is None:
        n_ders = abst_traj_n_ders
    a = [rand_vectors(n, np.array([-5.0]*abst_state_size), np.array([5.0]*abst_state_size))]

    for i in range(n_ders):
        a.append(rand_vectors(n, np.array([-20.0]*abst_state_size), np.array([20.0]*abst_state_size)))

    return np.stack(a, axis=1)

n_steps = int(t_f/dt)+1

if use_lr_search:
    swrm_abs_trainer = lambda lr: tf.train.AdamOptimizer(learning_rate=lr)
    swrm_pol_trainer = lambda lr: tf.train.AdamOptimizer(learning_rate=lr)
    prnt_pol_trainer = lambda lr: tf.train.AdamOptimizer(learning_rate=lr)
else:
    swrm_abs_trainer = tf.train.AdamOptimizer(learning_rate=0.001)
    swrm_pol_trainer = tf.train.AdamOptimizer(learning_rate=0.01)
    prnt_pol_trainer = tf.train.AdamOptimizer(learning_rate=0.001)


prnt_dyn_params = {}
prnt_dyn_params['del_t'] = dt
prnt_dyn_params['p_min'] = p_min
prnt_dyn_params['p_max'] = p_max
prnt_dyn_params['v_min'] = v_min
prnt_dyn_params['v_max'] = v_max
prnt_dyn_params['theta_min'] = theta_min
prnt_dyn_params['theta_max'] = theta_max
prnt_dyn_params['omega_min'] = omega_min
prnt_dyn_params['omega_max'] = omega_max
prnt_dyn_params['clip_p'] = False
prnt_dyn_params['clip_v'] = False
prnt_dyn_params['clip_theta'] = False
prnt_dyn_params['clip_omega'] = False

swrm_dyn_params = {}
swrm_dyn_params['del_t'] = dt
swrm_dyn_params['p_min'] = p_min
swrm_dyn_params['p_max'] = p_max
swrm_dyn_params['v_min'] = v_min
swrm_dyn_params['v_max'] = v_max
swrm_dyn_params['clip_p'] = False
swrm_dyn_params['clip_v'] = False

abst_enc_params = {}
abst_enc_params['max_swarm_size'] = max_swarm_size
abst_enc_params['a_limit'] = a_limit
abst_enc_params['init_var'] = 0.1
abst_enc_params['tran_h_size'] = 200
abst_enc_params['tran_use_bias'] = False
abst_enc_params['tran_activ'] = 'leakyrelu_0.1'
abst_enc_params['tran_dropout_rate'] = 0.2
abst_enc_params['comb_h_size'] = 200
abst_enc_params['comb_use_bias'] = False
abst_enc_params['comb_activ'] = 'leakyrelu_0.1'
abst_enc_params['comb_dropout_rate'] = 0.2
abst_enc_params['subsample'] = True

abst_dyn_params = {}
abst_dyn_params['dt'] = dt
abst_dyn_params['use_params'] = False
abst_dyn_params['hidden_size'] = 200
abst_dyn_params['hidden_activ'] = 'leakyrelu_0.1'
abst_dyn_params['use_bias'] = False
abst_dyn_params['init_var'] = 0.1

swrm_pol_params = {}
swrm_pol_params['h_size'] = 100
swrm_pol_params['use_bias'] = False
swrm_pol_params['activ'] = 'leakyrelu_0.1'
swrm_pol_params['a_min'] = [v_min]
swrm_pol_params['a_max'] = [v_max]
swrm_pol_params['n_ders'] = abst_traj_n_ders
swrm_pol_params['individualized'] = False
swrm_pol_params['parameterized'] = True
swrm_pol_params['max_swarm_size'] = max_swarm_size
swrm_pol_params['init_var'] = 0.1
swrm_pol_params['K'] = K_abs
swrm_pol_params['dt'] = dt

abst_lrn_params = {}
abst_lrn_params['max_swarm_size'] = max_swarm_size
abst_lrn_params['n_ders'] = abst_traj_n_ders
abst_lrn_params['K'] = K_abs
abst_lrn_params['dt'] = dt
abst_lrn_params['p_max'] = p_max
abst_lrn_params['p_min'] = p_min
#abst_lrn_params['l1_loss_abs'] = 0.01
abst_lrn_params['l2_loss_abs'] = 0.1
#abst_lrn_params['l1_loss_pol'] = 0.01
abst_lrn_params['l2_loss_pol'] = 0.1
abst_lrn_params['pol_scale'] = 1.0
#abst_lrn_params['trainer'] = swrm_abs_trainer
abst_lrn_params['trainer_abs'] = swrm_abs_trainer
abst_lrn_params['trainer_pol'] = swrm_pol_trainer
abst_lrn_params['swrm_state_gen'] = swrm_state_gen
abst_lrn_params['swrm_param_gen'] = swrm_param_gen
abst_lrn_params['prnt_state_gen'] = prnt_state_gen
abst_lrn_params['prnt_param_gen'] = prnt_param_gen
abst_lrn_params['abst_state_gen'] = abst_state_gen
abst_lrn_params['verbosity'] = verbosity
abst_lrn_params['regen_data'] = not no_regen
abst_lrn_params['patience'] = patience
abst_lrn_params['use_best'] = use_best
abst_lrn_params['use_lr_search'] = use_lr_search
abst_lrn_params['lr_patience'] = lr_patience
abst_lrn_params['lr_min'] = lr_min
abst_lrn_params['lr_max'] = lr_max
abst_lrn_params['lr_range'] = lr_range
