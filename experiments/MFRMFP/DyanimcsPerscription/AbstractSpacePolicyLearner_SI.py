"""
AbstractSpacePolicyLearner_SI.py

Learn an abstract space and policy
"""

import matplotlib.pyplot as plt

from youngknight.frameworks.AbstractionEncoder import AbstractEmbedderPolicy
from youngknight.frameworks.AbstractionEncoder.encoders import HierarchicalEncoder
from youngknight.frameworks.AbstractionEncoder.dynamics import DoubleIntegratorDynamics
from youngknight.frameworks.Shooting.policies import JacobianPol
from youngknight.models.Dynamics import MFRMFP_Plane, MFRMFP_Swarm_SI

import params.exp1_SI as exp

n_epochs= 10
n_batches = 1000
batch_size = 128
val_size = 512

if __name__ == '__main__':
    print('Setting up networks...')
    prt_dyn = MFRMFP_Plane(params=exp.prnt_dyn_params, home=exp.home, name='Embedding_Plane_Dynamics')
    swm_dyn = MFRMFP_Swarm_SI(params=exp.swrm_dyn_params, home=exp.home, name='Embedding_Swarm_Dynamics')
    abs_enc = HierarchicalEncoder(exp.abst_state_size, exp.chld_state_size, exp.chld_param_size, params=exp.abst_enc_params, home=exp.home, name='Abstract_Encoder')
    abs_dyn = DoubleIntegratorDynamics(exp.abst_state_size, exp.prnt_state_size, exp.prnt_param_size, params=exp.abst_dyn_params, home=exp.home, name='Abstract_Dynamics')
    abs_pol = JacobianPol((None, exp.chld_state_size), (None, exp.chld_actin_size), abs_enc.abs_size, (None, exp.chld_param_size), params=exp.swrm_pol_params, home=exp.home, name='swrm_pol')

    print('Setting up learner...')
    learner = AbstractEmbedderPolicy(abs_enc, abs_pol, abs_dyn, prt_dyn, swm_dyn, params=exp.abst_lrn_params, name='AbstractLearner')

    print('Training...')
    hist = learner.train(n_epochs, n_batches, batch_size, val_size)

    print('saving...')
    abs_enc.save(exp.enc_filename)
    abs_dyn.save(exp.dyn_filename)
    abs_pol.save(exp.swm_pol_filename)

    plt.figure()
    plt.plot(hist['ave_loss_abs'])
    plt.plot(hist['val_loss_abs'])
    plt.plot(hist['ave_loss_pol'])
    plt.plot(hist['val_loss_pol'])
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend(['abs loss ave', 'abs loss val', 'pol loss ave', 'pol loss val'])

    plt.show()