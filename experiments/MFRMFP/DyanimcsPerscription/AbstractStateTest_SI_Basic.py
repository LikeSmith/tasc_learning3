"""
AbstractStateTest.py

tests abstract state and abstract dynamics
"""

import numpy as np
import matplotlib.pyplot as plt
from tqdm import trange

from youngknight.frameworks.AbstractionEncoder.encoders import BasicEncoder
from youngknight.frameworks.AbstractionEncoder.dynamics import DoubleIntegratorDynamics
from youngknight.models.Dynamics import MFRMFP_Plane, MFRMFP_Swarm_SI

import params.exp2_SI as exp

K_p = [-1.9, -1.5]
K_s = [2.0, 1.0]
test_batch_size = 1

def f_smooth_stribeck(v):
    return exp.gamma_1*(np.tanh(exp.gamma_2*v) - np.tanh(exp.gamma_3*v)) + exp.gamma_4*np.tanh(exp.gamma_5*v) + exp.gamma_6*v

def ctrl(prnt_state, swrm_state, swrm_params):
    batch_size = prnt_state.shape[0]
    swarm_size = swrm_state.shape[1]

    theta = prnt_state[:, 0]
    omega = prnt_state[:, 1]

    p = swrm_state[:, :, 0]
    v = swrm_state[:, :, 1]

    m = swrm_params[:, :, 0]

    tau_s = np.zeros(batch_size)
    tau_s_dot = np.zeros(batch_size)
    J_s = np.zeros(batch_size)
    J_s_dot = np.zeros(batch_size)
    S_0 = np.zeros(batch_size)
    S_1 = np.zeros(batch_size)
    S_2 = np.zeros(batch_size)
    S_3 = np.zeros(batch_size)

    for i in range(swarm_size):
        tau_s += 9.81*m[:, i]*p[:, i]
        tau_s_dot += 9.81*m[:, i]*v[:, i]
        J_s += m[:, i]*p[:, i]**2
        J_s_dot += 2*m[:, i]*p[:, i]*v[:, i]
        S_0 += m[:, i]**2
        S_1 += m[:, i]**2*p[:, i]
        S_2 += m[:, i]**2*p[:, i]**2

        for j in range(i+1, swarm_size):
            S_3 += m[:, i]**2*m[:, j]**2*(p[:, i] - p[:, j])**2

    tau_d = -K_p[0]*theta - K_p[1]*omega
    tau_d_dot = -K_p[0]*omega - K_p[1]*(-np.cos(theta)*tau_s - omega*J_s_dot - f_smooth_stribeck(omega))/(exp.J + J_s)
    J_d = 0.0125*tau_d**2 + 0.025
    J_d_dot = 2*0.0125*tau_d*tau_d_dot

    tau_dot = K_s[0]*(tau_d - tau_s) + tau_d_dot
    J_dot = K_s[1]*(J_d - J_s) + J_d_dot

    u_s = np.zeros((batch_size, swarm_size, 1))

    for i in range(swarm_size):
        phi_1 = m[:, i]*S_2 - m[:, i]*p[:, i]*S_1
        phi_2 = 0.5*(m[:, i]*p[:, i]*S_0 - m[:, i]*S_1)

        u_s[:, i, 0] = (phi_1*tau_dot + phi_2*J_dot)/S_3

    return u_s

if __name__ == '__main__':
    print('Setting up networks...')
    prt_dyn = MFRMFP_Plane(params=exp.prnt_dyn_params, home=exp.home, name='Plane_Dynamics')
    swm_dyn = MFRMFP_Swarm_SI(params=exp.swrm_dyn_params, home=exp.home, name='Swarm_Dynamics')
    abs_enc = BasicEncoder(exp.abst_state_size, exp.chld_state_size, exp.chld_param_size, load_file=exp.enc_filename, home=exp.home, name='Abstract_Encoder')
    abs_dyn = DoubleIntegratorDynamics(exp.abst_state_size, exp.prnt_state_size, exp.prnt_param_size, load_file=exp.dyn_filename, home=exp.home, name='Abstract_Dynamics')

    s_p0 = exp.prnt_state_gen(test_batch_size)
    s_s0 = exp.swrm_state_gen(test_batch_size, exp.max_swarm_size)
    p_p = exp.prnt_param_gen(test_batch_size)
    p_s = exp.swrm_param_gen(test_batch_size, exp.max_swarm_size)
    #s_p0[0, 1] = 0.0

    t = np.arange(0.0, exp.t_f+exp.dt, exp.dt)
    s_p_true = np.zeros((test_batch_size, t.shape[0], 2))
    s_p_pred = np.zeros((test_batch_size, t.shape[0], 2))
    s_s = np.zeros((test_batch_size, t.shape[0], 16, 2))
    u_s = np.zeros((test_batch_size, t.shape[0]-1, 16, 1))
    a_s = np.zeros((test_batch_size, t.shape[0], exp.abst_state_size))

    s_p_true[:, 0, :] = s_p0
    s_p_pred[:, 0, :] = s_p0
    s_s[:, 0, :, :] = s_s0
    a_s[:, 0, :] = abs_enc.encode(s_s0, p_s)

    for i in trange(t.shape[0]-1):
        u_s[:, i, :, :] = ctrl(s_p_true[:, i, :], s_s[:, i, :, :], p_s)

        s_s[:, i+1, :, :] = swm_dyn.step(s_s[:, i, :, :], u_s[:, i, :, :], p_s, i)
        a_s[:, i+1, :] = abs_enc.encode(s_s[:, i+1, :, :], p_s)
        s_p_true[:, i+1, :] = prt_dyn.step(s_p_true[:, i, :], np.concatenate([s_s[:, i+1, :, :], p_s], axis=2), p_p, i)
        s_p_pred[:, i+1, :] = abs_dyn.step(s_p_true[:, i, :], a_s[:, i+1, :], p_p)

    plt.figure()
    plt.plot(t, s_s[0, :, :, 0])
    plt.xlabel('time (s)')
    plt.ylabel('p (m)')

    plt.figure()
    plt.plot(t, s_p_true[0, :, 0], 'b')
    plt.plot(t, s_p_pred[0, :, 0], 'b--')
    plt.xlabel('time (s)')
    plt.ylabel('theta (rad)')

    plt.figure()
    plt.plot(t, s_p_true[0, :, 1], 'b')
    plt.plot(t, s_p_pred[0, :, 1], 'b--')
    plt.xlabel('time (s)')
    plt.ylabel('omega (rad/s)')

    plt.figure()
    plt.plot(t, a_s[0, :, :])
    plt.xlabel('time (s)')
    plt.ylabel('abstract states')
