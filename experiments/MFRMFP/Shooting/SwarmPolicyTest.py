"""
SwarmPolicyTest.py

test of the learned policy
"""

import numpy as np
import _pickle as pkl
import matplotlib.pyplot as plt
from tqdm import trange

from youngknight.models.Dynamics import MFRMFP_Swarm_SI
from youngknight.frameworks.AbstractionEncoder.encoders import HierarchicalEncoder
#from youngknight.frameworks.AbstractionEncoder.encoders.constant_encoders import MassDistEncoder
from youngknight.frameworks.Shooting.policies import BasicPol_Swarm_AbsTrack

import params.exp_SI2 as exp

if __name__ == '__main__':
    print('Setting up networks...')
    dyn = MFRMFP_Swarm_SI(exp.swrm_dyn_params, home=exp.home, name='swrm_dyn')
    abs_enc = HierarchicalEncoder(exp.abst_state_size, exp.chld_state_size, exp.chld_param_size, load_file=exp.enc_filename, home=exp.home, name='abs_enc')
    #abs_enc = MassDistEncoder(2, exp.chld_state_size, exp.chld_param_size, load_file=exp.enc_filename, home=exp.home, name='abs_enc')
    pol = BasicPol_Swarm_AbsTrack((exp.max_swarm_size, exp.chld_state_size), (exp.max_swarm_size, exp.chld_actin_size), abs_enc.abs_size, (exp.max_swarm_size, exp.chld_param_size), load_file=exp.swm_pol_filename, home=exp.home, name='swrm_pol')

    traj_data, dt = pkl.load(open(exp.home+'Abst_traj_sin.pkl', 'rb'))
    traj_data= np.zeros(traj_data.shape)

    #outputs, loss, s0_val, p_val, s_des_val = pkl.load(open(exp.home+'val_results_epoch28.pkl', 'rb'))
    print('Setting up sim...')
    s0 = exp.swrm_state_gen(1, 16)
    p = exp.swrm_param_gen(1, 16)
    i = np.random.randint(traj_data.shape[0])

    t = np.arange(0.0, exp.t_f+exp.dt, exp.dt)
    n = t.shape[0]

    s_s = np.zeros((n, 16, 2))
    s_a = np.zeros((n, 4))
    s_d = traj_data[i, :, 0, :]

    s_s[0, :, :] = s0[0]
    s_a[0, :] = abs_enc.encode(s0, p)[0, :]

    for k in trange(n-1):
        s_cur = np.expand_dims(s_s[k, :, :], axis=0)
        s_abs = np.expand_dims(s_a[k, :], axis=0)
        a = pol.pol(s_cur, s_abs , traj_data[i:i+1, k, :, :], p, i)
        s_nxt = dyn.step(s_cur, a, p, k)
        s_abs_nxt = abs_enc.encode(s_nxt, p)

        s_s[k+1, :, :] = s_nxt[0, :, :]
        s_a[k+1, :] = s_abs_nxt[0, :]

    plt.figure()
    plt.plot(t, s_s[:, :, 0])

    plt.figure()
    plt.plot(t, s_s[:, :, 1])

    for i in range(4):
        plt.figure()
        plt.plot(t, s_a[:, i], 'b')
        plt.plot(t[:-1], s_d[:, i], 'b--')
