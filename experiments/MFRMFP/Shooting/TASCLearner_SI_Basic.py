"""
TASC_learning,py

Learning TASC architecture for SI swarm
"""

import numpy as np
import matplotlib.pyplot as plt

from youngknight.frameworks.AbstractionEncoder.encoders import BasicEncoder
from youngknight.frameworks.AbstractionEncoder.dynamics import DoubleIntegratorDynamics
from youngknight.frameworks.Shooting.policies import BasicPol_Swarm_AbsTrack, BasicPol
from youngknight.frameworks.Shooting import Shooting_AbsTrack
from youngknight.models.Dynamics import MFRMFP_Plane, MFRMFP_Swarm_SI

import params.exp_SI2 as exp

n_epochs= 10
n_batches = 1000
batch_size = 128
val_size = 512

if __name__ == '__main__':
    print('Setting up Dynamics...')
    prt_dyn = MFRMFP_Plane(params=exp.prnt_dyn_params, home=exp.home, name='Embedding_Plane_Dynamics')
    swm_dyn = MFRMFP_Swarm_SI(params=exp.swrm_dyn_params, home=exp.home, name='Embedding_Swarm_Dynamics')

    print('Setting up Networks...')
    abs_enc = BasicEncoder(exp.abst_state_size, exp.chld_state_size, exp.chld_param_size, params=exp.abst_enc_params, home=exp.home, name='abst_enc')
    abs_dyn = DoubleIntegratorDynamics(exp.abst_state_size, exp.prnt_state_size, exp.prnt_param_size, params=exp.abst_dyn_params, home=exp.home, name='abst_dyn')
    prt_pol = BasicPol((exp.prnt_state_size,), (exp.abst_state_size,), (exp.prnt_param_size,), params=exp.prnt_pol_params, home=exp.home, name='prnt_pol')
    swm_pol = BasicPol_Swarm_AbsTrack((None, exp.chld_state_size), (None, exp.chld_actin_size), abs_enc.abs_size, (None, exp.chld_param_size), params=exp.swrm_pol_params, home=exp.home, name='swrm_pol')

    print('Building Learner...')
    learner = Shooting_AbsTrack(abs_enc, abs_dyn, prt_pol, swm_pol, prt_dyn, swm_dyn, params=exp.full_trn_params, home=exp.home, name='learner_shooting')

    print('Training Networks...')
    hist = learner.train(n_epochs, n_batches, batch_size, val_size)

    print('plotting...')
    for key in learner.loss_ops.keys():
        plt.figure()
        plt.plot(hist['ave_%s'%key])
        plt.plot(hist['val_%s'%key])
        plt.xlabel('epoch')
        plt.legend(['taining %s'%key, 'validation %s'%key])

    plt.figure()
    plt.plot(np.arange(hist['m_hist'].shape[0]), hist['m_hist'])
    for up in hist['nans']:
        plt.gca().axvline(up, color='r')
    plt.xlabel('update')
    plt.ylabel('m')

    plt.figure()
    plt.plot(np.arange(hist['v_hist'].shape[0]), hist['v_hist'])
    for up in hist['nans']:
        plt.gca().axvline(up, color='r')
    plt.xlabel('update')
    plt.ylabel('v')
