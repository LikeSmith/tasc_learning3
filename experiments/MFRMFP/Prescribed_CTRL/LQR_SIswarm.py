'''
MFRMFP test
'''

import numpy as np
import matplotlib.pyplot as plt

from youngknight.sims import MFRMFP

K_p = [3.1, 3.3]
K_s = [10.0, 1.0]

dt = 0.05
swarm_size = 10
L = 1.0
horizon = 2000
n = 1

parent_limits = {}
parent_limits['theta_min'] = -0.5
parent_limits['theta_max'] = 0.5
parent_limits['omega_min'] = -0.1
parent_limits['omega_max'] = 0.1
parent_limits['mu_min'] = -5.0
parent_limits['mu_max'] = 5.0
parent_limits['J_min'] = 0.0
parent_limits['J_max'] = 5.0
parent_limits['J_dot_min'] = -5.0
parent_limits['J_dot_max'] = 5.0
parent_limits['J_p_min'] = 0.5
parent_limits['J_p_max'] = 1.0
parent_limits['gamma_1_min'] = 0.01
parent_limits['gamma_1_max'] = 0.09
parent_limits['gamma_2_min'] = 1000
parent_limits['gamma_2_max'] = 1000
parent_limits['gamma_23_min'] = 300
parent_limits['gamma_23_max'] = 300
parent_limits['gamma_14_min'] = 0.001
parent_limits['gamma_14_max'] = 0.009
parent_limits['gamma_5_min'] = 1000
parent_limits['gamma_5_max'] = 1000
parent_limits['gamma_6_min'] = 0.0
parent_limits['gamma_6_max'] = 0.0

child_limits = {}
child_limits['p_max'] = 0.25
child_limits['p_min'] = -0.25
child_limits['v_max'] = 1.0
child_limits['v_min'] = -1.0
child_limits['m_max'] = 0.5
child_limits['m_min'] = 0.25

def parent_ctrl(state, k, params, abs_state):
    J_p = params[:, 0]
    gamma_1 = params[:, 1]
    gamma_2 = params[:, 2]
    gamma_3 = params[:, 3]
    gamma_4 = params[:, 4]
    gamma_5 = params[:, 5]
    gamma_6 = params[:, 6]
    
    f_f = gamma_1*(np.tanh(gamma_2*state[:, 1]) - np.tanh(gamma_3*state[:, 1])) + gamma_4*np.tanh(gamma_5*state[:, 1]) + gamma_6*state[:, 1]
    
    n = state.shape[0]
    t = K_p[0]*state[:, 0] + K_p[1]*state[:, 1]
    J = 0.0125*t**2 + 0.025
    t_dot = K_p[0]*state[:, 1] + K_p[1]*(-np.cos(state[:, 0])*abs_state[:, 0] - state[:, 1]*abs_state[:, 2] - f_f)/(J_p + abs_state[:, 1])
    J_dot = 2*0.0125*t*t_dot
    
    a = np.zeros((n, 4))
    a[:, 0] = t
    a[:, 1] = J
    a[:, 2] = t_dot
    a[:, 3] = J_dot
    return a

def child_ctrl(state, abs_state, k, params, des_state):
    mu_d = des_state[:, 0]
    J_d = des_state[:, 1]
    mu_d_dot = des_state[:, 2]
    J_d_dot = des_state[:, 3]
    
    d_abs1 = K_s[0]*(mu_d - abs_state[:, 0]) + mu_d_dot
    d_abs2 = K_s[1]*(J_d - abs_state[:, 1]) + J_d_dot
    
    phi_1 = (params[:, 0]*abs_state[:, 5] - params[:, 0]*state[:, 0]*abs_state[:, 4])/abs_state[:, 6]
    phi_2 = (params[:, 0]*state[:, 0]*abs_state[:, 3] - params[:, 0]*abs_state[:, 4])/(2*abs_state[:, 6])
    
    return d_abs1*phi_1 + d_abs2*phi_2

if __name__ == '__main__':
    sim = MFRMFP(parent_limits=parent_limits, child_limits=child_limits, swarm_size=swarm_size, dt=dt, L=L, enforce_limits=False, parent_full_state_feedback=True, child_full_state_feedback=True, parent_ctrl=parent_ctrl, child_ctrl=child_ctrl, name='MFRMFP_test')
    
    res, p_p, c_p = sim.gen_trajectories(initial_parent_states=sim.parent_init_state_gen(n), initial_child_states=sim.child_init_state_gen(n), horizon=horizon, parent_params=sim.parent_param_gen(n), child_params=sim.child_param_gen(n))
    
    abs_states = np.zeros((horizon+1, 2))
    abs_states_d = np.zeros((horizon+1, 2))
    
    for i in range(horizon+1):
        a = sim.abs_calc(res['child_states'][:, i, :, :], i, c_p)
        abs_states[i, :] = a[0, :2]
        abs_states_d[i, :] = parent_ctrl(res['parent_states'][:, i, :], i, p_p, a)[0, :2]
        
    t = np.arange(horizon+1)*dt
    
    plt.figure()
    plt.plot(t, res['parent_states'][:, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('theta (rad)')
    
    plt.figure()
    plt.plot(t, res['parent_states'][:, :, 1].T)
    plt.xlabel('time (s)')
    plt.ylabel('omega (rad/s)')
    
    plt.figure()
    plt.plot(t, res['child_states'][0, :, :, 0])
    plt.xlabel('time (s)')
    plt.ylabel('positions (m)')
    
    plt.figure()
    plt.plot(t, abs_states)
    plt.plot(t, abs_states_d)
    plt.xlabel('time (s)')
    plt.title('abstract states')
    